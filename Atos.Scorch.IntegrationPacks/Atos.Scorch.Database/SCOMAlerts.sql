USE [SCOMAlerts]
GO
/****** Object:  User [SCOMAlertUser]    Script Date: 12/11/2014 13:13:34 ******/
CREATE USER [SCOMAlertUser] FOR LOGIN [SCOMAlertUser] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[TicketStatus]    Script Date: 12/11/2014 13:13:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Status] [nvarchar](50) NULL,
 CONSTRAINT [PK_dbo.TicketStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Settings]    Script Date: 12/11/2014 13:13:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[SettingName] [nvarchar](100) NULL,
	[SettingValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Settings] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OpCats]    Script Date: 12/11/2014 13:13:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OpCats](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](50) NULL,
	[Type] [nvarchar](50) NULL,
	[Item] [nvarchar](50) NULL,
	[Value] [nvarchar](50) NULL,
 CONSTRAINT [PK_dbo.OpCats] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ITSMTemplateMapping]    Script Date: 12/11/2014 13:13:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ITSMTemplateMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ManagementPack] [nvarchar](50) NULL,
	[Template] [nvarchar](50) NULL,
 CONSTRAINT [PK_dbo.ITSMTemplateMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Alert]    Script Date: 12/11/2014 13:13:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Alert](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Domain] [nvarchar](100) NULL,
	[Name] [nvarchar](100) NULL,
	[NetbiosComputerName] [nvarchar](100) NULL,
	[NetbiosDomainName] [nvarchar](100) NULL,
	[Owner] [nvarchar](100) NULL,
	[PrincipalName] [nvarchar](100) NULL,
	[Priority] [nvarchar](50) NULL,
	[ResolvedBy] [nvarchar](100) NULL,
	[Severity] [nvarchar](50) NULL,
	[SiteName] [nvarchar](100) NULL,
	[TimeAlertAdded] [datetime] NULL,
	[TimeAlertResolved] [datetime] NULL,
	[TimeAlertRaised] [datetime] NULL,
	[Date_Added] [datetime] NULL,
	[Added_By] [nvarchar](100) NULL,
	[Date_Last_Modified] [datetime] NULL,
	[Last_Modified_By] [nvarchar](100) NULL,
	[AlertCount] [int] NOT NULL,
	[Category] [nvarchar](100) NULL,
	[ClassId] [uniqueidentifier] NOT NULL,
	[Connection] [nvarchar](100) NULL,
	[CompanyKnowledge] [nvarchar](max) NULL,
	[ConnectorId] [uniqueidentifier] NOT NULL,
	[ConnectorStatus] [nvarchar](100) NULL,
	[Context] [nvarchar](100) NULL,
	[CustomField1] [nvarchar](100) NULL,
	[CustomField2] [nvarchar](100) NULL,
	[CustomField3] [nvarchar](100) NULL,
	[CustomField4] [nvarchar](100) NULL,
	[CustomField5] [nvarchar](100) NULL,
	[CustomField6] [nvarchar](100) NULL,
	[CustomField7] [nvarchar](100) NULL,
	[CustomField8] [nvarchar](100) NULL,
	[CustomField9] [nvarchar](100) NULL,
	[CustomField10] [nvarchar](100) NULL,
	[IsMonitorAlert] [bit] NOT NULL,
	[LastModifiedByNonConnector] [bit] NOT NULL,
	[MaintenanceModeLastModified] [datetime] NULL,
	[ManagementGroup] [nvarchar](100) NULL,
	[ManagementGroupId] [uniqueidentifier] NOT NULL,
	[ManagementPackDisplayName] [nvarchar](100) NULL,
	[ManagementPackFriendlyName] [nvarchar](100) NULL,
	[ManagementPackId] [uniqueidentifier] NOT NULL,
	[ManagementPackIsSealed] [bit] NOT NULL,
	[ManagementPackMonitorName] [nvarchar](100) NULL,
	[ManagementPackName] [nvarchar](100) NULL,
	[ManagementPackVersion] [nvarchar](100) NULL,
	[MonitoringClassId] [uniqueidentifier] NOT NULL,
	[MonitoringObjectDisplayName] [nvarchar](100) NULL,
	[MonitoringObjectFullName] [nvarchar](200) NULL,
	[MonitoringObjectHealthState] [nvarchar](100) NULL,
	[MonitoringObjectId] [uniqueidentifier] NOT NULL,
	[MonitoringObjectInMaintenanceMode] [bit] NOT NULL,
	[MonitoringObjectName] [nvarchar](100) NULL,
	[MonitoringObjectPath] [nvarchar](100) NULL,
	[MonitoringRuleId] [uniqueidentifier] NOT NULL,
	[MonitoringRuleName] [nvarchar](100) NULL,
	[Parameters] [nvarchar](255) NULL,
	[ProblemId] [uniqueidentifier] NOT NULL,
	[RepeatCount] [int] NOT NULL,
	[ResolutionState] [int] NOT NULL,
	[RuleId] [uniqueidentifier] NOT NULL,
	[Server] [nvarchar](100) NULL,
	[StateLastModified] [datetime] NULL,
	[TimeResolutionStateLastModified] [datetime] NULL,
	[Username] [nvarchar](100) NULL,
	[ProductKnowledge] [nvarchar](max) NULL,
	[AlertId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_dbo.Alert] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ITSMRequestMessage]    Script Date: 12/11/2014 13:13:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ITSMRequestMessage](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[SRDTitleId] [int] NULL,
	[Summary] [nvarchar](max) NULL,
	[Company] [nvarchar](50) NULL,
	[SourceKeyword] [nvarchar](50) NULL,
	[StatusId] [int] NOT NULL,
	[Urgency] [nvarchar](50) NULL,
	[Impact] [nvarchar](50) NULL,
	[ShortDescription] [nvarchar](max) NULL,
	[SubmitterId] [nvarchar](50) NULL,
	[RequesterId] [nvarchar](50) NULL,
	[WorkInfoType] [nvarchar](30) NULL,
	[WorkInfoCommSrc] [nvarchar](30) NULL,
	[WorkInfoSummary] [nvarchar](30) NULL,
	[WorkInfoDetails] [nvarchar](30) NULL,
	[Attachment1Name] [nvarchar](500) NULL,
	[Attachment1Data] [nvarchar](max) NULL,
	[AttachmentOrigSize] [int] NULL,
	[Date_Added] [datetime] NULL,
	[Added_By] [nvarchar](50) NULL,
	[TicketStatusId] [int] NULL,
	[FK_Request_Alert] [int] NOT NULL,
	[ITSMResponseMessageId] [int] NULL,
 CONSTRAINT [PK_dbo.ITSMRequestMessage] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[get_setting_value_from_name]    Script Date: 12/11/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[get_setting_value_from_name]
    @SettingName [nvarchar](100)
AS
BEGIN
     
    		        SET NOCOUNT ON;
    
    	            SELECT isnull(SettingValue,'') from Settings where SettingName = @SettingName
END
GO
/****** Object:  Table [dbo].[ITSMResponseMessage]    Script Date: 12/11/2014 13:13:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ITSMResponseMessage](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ResultCode] [nvarchar](100) NULL,
	[ResultMessage] [nvarchar](max) NULL,
	[MasterId] [uniqueidentifier] NULL,
	[Date_Added] [datetime] NULL,
	[Added_By] [nvarchar](50) NULL,
	[ITSMRequestMessageId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.ITSMResponseMessage] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Default [DF__Alert__AlertCoun__182C9B23]    Script Date: 12/11/2014 13:13:27 ******/
ALTER TABLE [dbo].[Alert] ADD  DEFAULT ((0)) FOR [AlertCount]
GO
/****** Object:  Default [DF__Alert__IsMonitor__1920BF5C]    Script Date: 12/11/2014 13:13:27 ******/
ALTER TABLE [dbo].[Alert] ADD  DEFAULT ((0)) FOR [IsMonitorAlert]
GO
/****** Object:  Default [DF__Alert__LastModif__1A14E395]    Script Date: 12/11/2014 13:13:27 ******/
ALTER TABLE [dbo].[Alert] ADD  DEFAULT ((0)) FOR [LastModifiedByNonConnector]
GO
/****** Object:  Default [DF__Alert__Managemen__1B0907CE]    Script Date: 12/11/2014 13:13:27 ******/
ALTER TABLE [dbo].[Alert] ADD  DEFAULT ((0)) FOR [ManagementPackIsSealed]
GO
/****** Object:  Default [DF__Alert__Monitorin__1BFD2C07]    Script Date: 12/11/2014 13:13:27 ******/
ALTER TABLE [dbo].[Alert] ADD  DEFAULT ((0)) FOR [MonitoringObjectInMaintenanceMode]
GO
/****** Object:  Default [DF__Alert__RepeatCou__1CF15040]    Script Date: 12/11/2014 13:13:27 ******/
ALTER TABLE [dbo].[Alert] ADD  DEFAULT ((0)) FOR [RepeatCount]
GO
/****** Object:  Default [DF__ITSMReque__FK_Re__1DE57479]    Script Date: 12/11/2014 13:13:27 ******/
ALTER TABLE [dbo].[ITSMRequestMessage] ADD  DEFAULT ((0)) FOR [FK_Request_Alert]
GO
/****** Object:  Default [DF__ITSMRespo__ITSMR__1ED998B2]    Script Date: 12/11/2014 13:13:27 ******/
ALTER TABLE [dbo].[ITSMResponseMessage] ADD  DEFAULT ((0)) FOR [ITSMRequestMessageId]
GO
/****** Object:  ForeignKey [FK_dbo.ITSMRequestMessage_dbo.Alert_FK_Request_Alert]    Script Date: 12/11/2014 13:13:27 ******/
ALTER TABLE [dbo].[ITSMRequestMessage]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ITSMRequestMessage_dbo.Alert_FK_Request_Alert] FOREIGN KEY([FK_Request_Alert])
REFERENCES [dbo].[Alert] ([Id])
GO
ALTER TABLE [dbo].[ITSMRequestMessage] CHECK CONSTRAINT [FK_dbo.ITSMRequestMessage_dbo.Alert_FK_Request_Alert]
GO
/****** Object:  ForeignKey [FK_dbo.ITSMResponseMessage_dbo.ITSMRequestMessage_ITSMRequestMessageId]    Script Date: 12/11/2014 13:13:27 ******/
ALTER TABLE [dbo].[ITSMResponseMessage]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ITSMResponseMessage_dbo.ITSMRequestMessage_ITSMRequestMessageId] FOREIGN KEY([ITSMRequestMessageId])
REFERENCES [dbo].[ITSMRequestMessage] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ITSMResponseMessage] CHECK CONSTRAINT [FK_dbo.ITSMResponseMessage_dbo.ITSMRequestMessage_ITSMRequestMessageId]
GO
