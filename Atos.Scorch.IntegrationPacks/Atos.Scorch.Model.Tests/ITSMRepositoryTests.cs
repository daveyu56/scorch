﻿
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Atos.Scorch.WebApi.Host.Directors;
using Atos.Scorch.Model.Repositorys;
using Atos.Scorch.Domain;

namespace Atos.Scorch.Model.Tests
{
    [TestClass]
    public class ITSMRepositoryTests
    {
        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_Exchange_Ignore_Alert()
        {
            var director = new ITSMDirector(new ITSMRepository());



            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"#INTERNAL Test#1";
            alert.TimeAdded = new DateTime(2015, 3, 23, 18, 15, 0);
            alert.ManagementPackName = @"Microsoft.Exchange.15.Addendum";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"BGB01XH1001 - IMAP";
            alert.MonitoringObjectFullName = @"Microsoft.Windows.OperatingSystem:SDCBBC-WOTXD001.national.core.bbc.co.uk";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.MonitoringObjectPath = @"BGB01XH1001";
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = @"#INTERNAL test#1";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_Exchange_Critical_OOH()
        {
            var director = new ITSMDirector(new ITSMRepository());



            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"#INTERNAL Test#1";
            alert.TimeAdded = new DateTime(2015, 3, 23, 18, 15, 0);
            alert.ManagementPackName = @"Microsoft.Exchange.15.Addendum";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"Microsoft Windows Server 2008 R2 Standard";
            alert.MonitoringObjectFullName = @"Microsoft.Windows.OperatingSystem:SDCBBC-WOTXD001.national.core.bbc.co.uk";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.MonitoringObjectPath = @"SDCBBC-WOTXD002.national.core.bbc.co.uk;MSSQLSERVER;ECR";
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = @"#INTERNAL test#1";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_Exchange_Critical_InHours()
        {
            var director = new ITSMDirector(new ITSMRepository());



            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"#INTERNAL Test#1";
            alert.TimeAdded = new DateTime(2015, 3, 23, 10, 15, 0);
            alert.ManagementPackName = @"Microsoft.Exchange.15.Addendum";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"Microsoft Windows Server 2008 R2 Standard";
            alert.MonitoringObjectFullName = @"Microsoft.Windows.OperatingSystem:SDCBBC-WOTXD001.national.core.bbc.co.uk";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.MonitoringObjectPath = @"SDCBBC-WOTXD002.national.core.bbc.co.uk;MSSQLSERVER;ECR";
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = @"#INTERNAL test#1";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_Exchange_Warning_InHours()
        {
            var director = new ITSMDirector(new ITSMRepository());



            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"#INTERNAL Test#1";
            alert.TimeAdded = new DateTime(2015, 3, 23, 10, 15, 0);
            alert.ManagementPackName = @"Microsoft.Exchange.15.Addendum";
            alert.Priority = "Medium";
            alert.Severity = "Warning";
            alert.MonitoringObjectDisplayName = @"Microsoft Windows Server 2008 R2 Standard";
            alert.MonitoringObjectFullName = @"Microsoft.Windows.OperatingSystem:SDCBBC-WOTXD001.national.core.bbc.co.uk";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.MonitoringObjectPath = @"SDCBBC-WOTXD002.national.core.bbc.co.uk;MSSQLSERVER;ECR";
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = @"#INTERNAL test#1";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_SQL_Named_Server_Critical_InHours_Extra()
        {
            var director = new ITSMDirector(new ITSMRepository());



            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"#INTERNAL Test#1";
            alert.TimeAdded = new DateTime(2015, 3, 23, 10, 15, 0);
            alert.ManagementPackName = @"Microsoft.SQLServer.2008.Mirroring.Monitoring";
            alert.Priority = "Medium";
            alert.Severity = "Warning";
            alert.MonitoringObjectDisplayName = @"Microsoft Windows Server 2008 R2 Standard";
            alert.MonitoringObjectFullName = @"Microsoft.Windows.OperatingSystem:SDCBBC-WOTXD001.national.core.bbc.co.uk";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.MonitoringObjectPath = @"SDCBBC-WOTXD002.national.core.bbc.co.uk;MSSQLSERVER;ECR";
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = @"#INTERNAL test#1";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_Windows_Critical_NonProduction()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.ManagementPackName = @"Microsoft.SQLServer.2008.Monitoring";
            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"SQL Server cannot authenticate using Kerberos because the Service Principal Name (SPN) is missing, misplaced, or duplicated.
        Service Account: NT AUTHORITY\NETWORKSERVICE
        Missing SPNs: MSSQLSvc/BGB01AP1233.national.core.bbc.co.uk:49479
        Misplaced SPNs: 
        Duplicate SPNs: ";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 3, 23);
            alert.MonitoringObjectDisplayName = @"Microsoft.SQLServer.2008.Monitoring";
            alert.MonitoringObjectFullName = @"Microsoft.SQLServer.DBEngine:BGB01AP1233.national.core.bbc.co.uk;DOKUSTAR";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.MonitoringObjectName = @"DOKUSTAR";
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Status = 0;
            alert.Priority = "Medium";
            alert.Severity = "Warning";
            alert.Name = "SQL Server cannot authenticate using Kerberos because the Service Principal Name (SPN) is missing, misplaced, or duplicated.";

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_Unix_Critical_NonProduction()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 3, 23);
            alert.ManagementPackName = @"Microsoft.Linux.RHEL.6";
            alert.MonitoringObjectDisplayName = @"/app/scorch/test";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.MonitoringObjectName = "BGBGWAP1000";
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Status = 0;
            alert.Priority = "High";
            alert.Severity = "Critical";
            alert.Name = "Unix Critical Production Server AlertTest 1 heartbeat failed ";

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_Unix_Critical_Production()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Management Health Service on computer BGB01AP1348.national.core.bbc.co.uk failed to heartbeat.";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 3, 23);
            alert.ManagementPackName = @"Microsoft.SystemCenter.2007";
            alert.MonitoringObjectDisplayName = @"BGB01AP1348.national.core.bbc.co.uk";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.MonitoringObjectName = "UGBGWSC9999";
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Status = 0;
            alert.Priority = "High";
            alert.Severity = "Critical";
            alert.Name = "Unix Critical Production Server AlertTest 1 heartbeat failure";

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_VMWare_vSphere_Critical_IOH()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 8, 24, 12, 3, 23);
            alert.ManagementPackName = @"AO.VMWare.vSphere";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"/var/avamar";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.MonitoringObjectName = "SAPPRDPE0DB01";
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Alert Unix Test 1 default template";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_VMWare_vSphere()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 8, 24, 12, 3, 23);
            alert.ManagementPackName = @"AO.VMWare.vSphere";
            alert.Priority = "Medium";
            alert.Severity = "Warning";
            alert.MonitoringObjectDisplayName = @"/var/avamar";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.MonitoringObjectName = "SAPPRDPE0DB01";
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Alert Unix Test 1 default template";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_Unix_Avamar_Critical_OOH()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 19, 3, 23);
            alert.ManagementPackName = @"Microsoft.Linux.RHEL.6";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"/var/avamar";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Alert Unix Test 1 default template";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_Unix_SAP_Critical_OOH()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 19, 3, 23);
            alert.ManagementPackName = @"Microsoft.Linux.RHEL.6";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"/usr/sap/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Alert Unix Test 1 default template";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_Unix_Avamar_Warning()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 3, 23);
            alert.ManagementPackName = @"Microsoft.Linux.RHEL.6";
            alert.Priority = "Medium";
            alert.Severity = "Warning";
            alert.MonitoringObjectDisplayName = @"/var/avamar";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Alert Unix Test 1 default template";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_Unix_SAP_Warning()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 3, 23);
            alert.ManagementPackName = @"Microsoft.Linux.RHEL.6";
            alert.Priority = "Medium";
            alert.Severity = "Warning";
            alert.MonitoringObjectDisplayName = @"/usr/sap/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Alert Unix Test 1 default template";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_SQL_Named_Server_Critical_OOH()
        {
            var director = new ITSMDirector(new ITSMRepository());



            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"#INTERNAL Test#";
            alert.TimeAdded = new DateTime(2015, 3, 23, 19, 1, 0);
            alert.ManagementPackName = @"Microsoft.SQLServer.2008.Mirroring.Monitoring";
            alert.Priority = "Medium";
            alert.Severity = "Warning";
            alert.MonitoringObjectDisplayName = @"Microsoft Windows Server 2008 R2 Standard";
            alert.MonitoringObjectFullName = @"Microsoft.Windows.OperatingSystem:SDCBBC-WOTXD001.national.core.bbc.co.uk";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.MonitoringObjectPath = @"SDCBBC-WOTXD001.national.core.bbc.co.uk";
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = @"#INTERNAL test#";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }


        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_SQL_Named_Server_Critical()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 3, 23);
            alert.ManagementPackName = @"Microsoft.SQLServer.Generic.Dashboards";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"BGBGWAP1003";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Alert Test SQL Default test 1";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_SQL_Named_Server_Warning()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 3, 23);
            alert.ManagementPackName = @"Microsoft.SQLServer.Generic.Dashboards";
            alert.Priority = "Medium";
            alert.Severity = "Warning";
            alert.MonitoringObjectDisplayName = @"BGBGWAP1003";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Alert Test SQL Default test 1";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_SQL_Default_Critical_OOH()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 7, 59, 0);
            alert.ManagementPackName = @"Microsoft.SQLServer.Generic.Dashboards";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"UGBGWDB1030";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Alert Test SQL Default test 1";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_SQL_Default_Critical()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 3, 23);
            alert.ManagementPackName = @"Microsoft.SQLServer.Generic.Dashboards";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"UGBGWDB1030";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Alert Test SQL Default test 1";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_SQL_Default_Warning()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 3, 23);
            alert.ManagementPackName = @"Microsoft.SQLServer.Generic.Dashboards";
            alert.Priority = "Medium";
            alert.Severity = "Warning";
            alert.MonitoringObjectDisplayName = @"UGBGWDB1030";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Alert Test SQL Default test 1";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_Unix_Default_Critical_OOH()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 3, 30, 0);
            alert.ManagementPackName = @"Microsoft.Linux.RHEL.6";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"/app/scorch/test";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Alert Unix Test 1 default template";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_Unix_Default_Critical()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 3, 23);
            alert.ManagementPackName = @"Microsoft.Linux.RHEL.6";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"/app/scorch/test";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Alert Unix Test 1 default template";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_Unix_Default_Warning()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 3, 23);
            alert.ManagementPackName = @"Microsoft.Linux.RHEL.6";
            alert.Priority = "Medium";
            alert.Severity = "Warning";
            alert.MonitoringObjectDisplayName = @"/app/scorch/test";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Alert Unix Test 1 default template";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_RecoverPoint_Adaptor_Monitoring_Critical_OOH()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 19, 20, 0);
            alert.ManagementPackName = @"AO.RecoverPoint.Adapter.Monitoring";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"/var/avamar/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Avamar Alert Test 3";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }


        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_RecoverPoint_Adaptor_Monitoring_Critical()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 20, 0);
            alert.ManagementPackName = @"AO.RecoverPoint.Adapter.Monitoring";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"/var/avamar/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Avamar Alert Test 3";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_RecoverPoint_Adaptor_Monitoring_Warning()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 20, 0);
            alert.ManagementPackName = @"AO.RecoverPoint.Adapter.Monitoring";
            alert.Priority = "Medium";
            alert.Severity = "Warning";
            alert.MonitoringObjectDisplayName = @"/var/avamar/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Avamar Alert Test 3";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_DataDomain_Monitoring_Critical_OOH()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 23, 20, 0);
            alert.ManagementPackName = @"AO.DataDomain.Monitoring";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"/var/avamar/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Avamar Alert Test 2";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_DataDomain_Monitoring_Critical()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 20, 0);
            alert.ManagementPackName = @"AO.DataDomain.Monitoring";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"/var/avamar/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Avamar Alert Test 2";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_DataDomain_Monitoring_Warning()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 20, 0);
            alert.ManagementPackName = @"AO.DataDomain.Monitoring";
            alert.Priority = "Medium";
            alert.Severity = "Warning";
            alert.MonitoringObjectDisplayName = @"/var/avamar/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Avamar Alert Test 2";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_Avamar_Critical_OOH()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 8, 0, 0);
            alert.ManagementPackName = @"AO.Avamar.Monitoring";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"/var/avamar/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Avamar Alert Test 1";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_Avamar_Critical()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 0, 0);
            alert.ManagementPackName = @"AO.Avamar.Monitoring";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"/var/avamar/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Avamar Alert Test 1";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_Avamar()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 0, 0);
            alert.ManagementPackName = @"AO.Avamar.Monitoring";
            alert.Priority = "Medium";
            alert.Severity = "Warning";
            alert.MonitoringObjectDisplayName = @"/var/avamar/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = "Avamar Alert Test 1";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_Unix_Avamar_Warning_Saturday()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 8, 1, 12, 0, 0);
            alert.ManagementPackName = @"Microsoft.Linux.RHEL.6";
            alert.Priority = "Medium";
            alert.Severity = "Warning";
            alert.MonitoringObjectDisplayName = @"/var/avamar/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_Unix_Avamar_Saturday()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 8, 1, 12, 0, 0);
            alert.ManagementPackName = @"Microsoft.Linux.RHEL.6";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"/var/avamar/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_Unix_Avamar_BH()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 8, 31, 12, 0, 0);
            alert.ManagementPackName = @"Microsoft.Linux.RHEL.6";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"/var/avamar/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_Unix_Avamar_OOH()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 23, 0, 0);
            alert.ManagementPackName = @"Microsoft.Linux.RHEL.6";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"/var/avamar/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_Unix_Avamar()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 0, 0);
            alert.ManagementPackName = @"Microsoft.Linux.RHEL.6";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"/var/avamar/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_Unix_SAP_OOH()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 8, 0, 0);
            alert.ManagementPackName = @"Microsoft.Linux.RHEL.6";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"/usr/sap/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Avamar_Alert()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.ManagementPackName = @"AO.Avamar.Monitoring";
            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 3, 23);
            alert.MessageId = Guid.NewGuid();
            alert.Status = 0;
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.Name = "SCOM AO Avamar ALERT TEST 1";

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Avamar_Alert_OOH()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.ManagementPackName = @"AO.Avamar.Monitoring";
            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 18, 0, 0);
            alert.MessageId = Guid.NewGuid();
            alert.Status = 0;
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.Name = "SCOM AO Avamar ALERT TEST 1";

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }


        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_SCOM_Critical_OOH()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.ManagementPackName = @"Microsoft.SystemCenter.2007";
            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 2, 0, 0);
            alert.MonitoringObjectDisplayName = @"/usr/sap/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Status = 0;
            alert.Priority = "High";
            alert.Severity = "Critical";
            alert.Name = "SCOM PRIORITY ALERT TEST 1";

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_Windows_Critical_OOH()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.ManagementPackName = @"Microsoft.Windows.Server.2008.R2.Monitoring.BPA";
            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 18, 0, 0);
            alert.MonitoringObjectDisplayName = @"/usr/sap/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Status = 0;
            alert.Priority = "High";
            alert.Severity = "Critical";
            alert.Name = "SCOM PRIORITY ALERT TEST 1";

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_Windows_Critical_Production()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.ManagementPackName = @"Microsoft.Windows.Server.2008.R2.Monitoring.BPA";
            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 3, 23);
            alert.MonitoringObjectDisplayName = @"/usr/sap/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.MonitoringObjectName = "UGBGWSC1034";
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Status = 0;
            alert.Priority = "High";
            alert.Severity = "Critical";
            alert.Name = "SCOM Windows Critical Production Server AlertTest 1 heartbeat failed ";

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }


        [TestMethod]
        public void Scorch_Post_To_ITSM_New_Alert_Windows_Warning ()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.ManagementPackName = @"Microsoft.Windows.Server.2008.R2.Monitoring.BPA";
            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 3, 23);
            alert.MonitoringObjectDisplayName = @"/usr/sap/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Status = 0;
            alert.Priority = "High";
            alert.Severity = "Warning";
            alert.Name = "SCOM PRIORITY ALERT TEST 1";

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");

            
        }


        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_Unprocessed()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = new Guid("428CDA7D-EE1A-4893-8450-E5CA51D56F5F");
            alert.MessageId = Guid.NewGuid();
            alert.ManagementPackName = @"Microsoft.Windows.Server.2008.R2.Monitoring.BPA";
            alert.ManagementPackFriendlyName = @"Windows Server 2008 R2 Best Practice Analyzer Monitoring";
            alert.Name = "Test Alert 01 Name";

            alert.Server = "Test Alert 01 Server";
            alert.AlertDescription = "Test Alert 01 Description";

            alert.TimeAdded = DateTime.Now;

            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_Processed()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = new Guid("611A7F8C-3CC9-4100-AA6A-62D2C9D737EF");
            alert.MessageId = Guid.NewGuid();
            alert.ManagementPackName = @"Microsoft.Windows.Server.2008.R2.Monitoring.BPA";
            alert.ManagementPackFriendlyName = @"Windows Server 2008 R2 Best Practice Analyzer Monitoring";
            alert.Name = "Test Alert 01 Name";

            alert.Server = "Test Alert 01 Server";
            alert.AlertDescription = "Test Alert 01 Description";

            alert.TimeAdded = DateTime.Now;

            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "1");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_Unix_SAP()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 3, 23);
            alert.ManagementPackName = @"Microsoft.Linux.RHEL.6";
            alert.Priority = "Medium";
            alert.Severity = "Critical";
            alert.MonitoringObjectDisplayName = @"/usr/sap/";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();                  
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }



        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_SQL_Allow()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 3, 23);
            alert.ManagementPackName = @"Microsoft.SQLServer.Generic.Dashboards";
            alert.MonitoringObjectDisplayName = @"UGBGWDB1030";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_SQL__Dont_Allow()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 3, 23);
            alert.ManagementPackName = @"Microsoft.SQLServer.Generic.Dashboards";
            alert.MonitoringObjectDisplayName = @"BGB01AP1232";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "0");


        }

        [TestMethod]
        public void Scorch_Post_To_ITSM_Existing_Alert_Unix_Ignore()
        {
            var director = new ITSMDirector(new ITSMRepository());

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The System Center Orchestrator Runbook Server Monitor Service has logged a database connectivity error. Check the status of the SQL database.             Event Description: 03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:55:54 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 11:56:46 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode>Cannot open DB connection</MsgCode>  03/23/2015 12:27:53 <MsgCode Name: System Center Orchestrator Runbook Server Monitor Service Database Connection Error";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 3, 23);
            alert.ManagementPackName = @"Microsoft.Linux.RHEL.6";
            alert.MonitoringObjectDisplayName = @"/cdrom";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.2012.Orchestrator.Role.ManagementServer:UGBGWSC1034.national.core.bbc.co.uk;""D:\Program Files (x86)\Microsoft System Center 2012 R2\Orchestrator\Management Server\ManagementService.exe""";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);

            Assert.AreEqual(status.ResultCode, "1");

        }

    }
}
