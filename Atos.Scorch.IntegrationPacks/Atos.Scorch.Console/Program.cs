﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atos.Scorch.Model;
using System.Data.Entity;
using System.Threading;
using System.Diagnostics;
using Atos.Scorch.WebApi.Host.Directors;
using Atos.Scorch.Domain;
using Atos.Scorch.Model.Repositorys;

namespace Atos.Scorch.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var director = new ITSMDirector(new ITSMRepository());



            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"#INTERNAL Test#1";
            alert.TimeAdded = new DateTime(2015, 3, 23, 23, 18, 0);
            alert.ManagementPackName = @"Microsoft.SQLServer.2008.Mirroring.Monitoring";
            alert.Priority = "Medium";
            alert.Severity = "Warning";
            alert.MonitoringObjectDisplayName = @"Microsoft Windows Server 2008 R2 Standard";
            alert.MonitoringObjectFullName = @"Microsoft.Windows.OperatingSystem:SDCBBC-WOTXD001.national.core.bbc.co.uk";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.MonitoringObjectPath = @"SDCBBC-WOTXD002.national.core.bbc.co.uk;MSSQLSERVER;tempdb";
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = @"#INTERNAL test#1";
            alert.Status = 0;

            ITSMResponse status = director.PostAlertToITSM(alert);


           
        }
    }
}
