﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Security.Principal;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace Atos.Scorch.Logging
{
    public class Log
    {
        public const string LOG_TITLE = "Atos.Scorch";
        public const string LOG_PROCESS_CATEGORY = "Process";
        public const string LOG_AUDIT_CATEGORY = "Audit";
        public const string LOG_INFO_CATEGORY = "Information";
        public const string LOG_EXCEPTION_CATEGORY = "Exception";
        public const string LOG_CRITICAL_EXCEPTION_POLICY = "Critical";
        public const string LOG_DEFAULT_EXCEPTION_POLICY = "Default";


        static Log()
        {
            DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory());
            Logger.SetLogWriter(new LogWriterFactory().Create());
            IConfigurationSource config = ConfigurationSourceFactory.Create();
           

        }

        public static void Exception(Exception ex)
        {
            Exception(null, ex);
        }

        public static void Exception(string message, Exception ex)
        {
            StringBuilder sb = new StringBuilder();

            if (message != null)
            {
                sb.Append(message + "\n");
            }

            Exception thisEx = ex;
            do
            {
                sb.Append(ex.Message + "\n");
                sb.Append(ex.StackTrace + "\n");

                ex = ex.InnerException;

                if (ex != null)
                {
                    sb.Append("\nInner Exception:\n");
                }
            } while (ex != null);

            Logger.Write(sb.ToString(),
                LOG_EXCEPTION_CATEGORY,
                1, 2,
                System.Diagnostics.TraceEventType.Warning,
                LOG_TITLE);
        }

        public static void Info(string message)
        {
            Logger.Write(message,
                LOG_INFO_CATEGORY,
                1, 2,
                System.Diagnostics.TraceEventType.Information,
                LOG_TITLE);
        }

        public static void Audit(string message)
        {
            Audit(message, System.Diagnostics.TraceEventType.Information);
        }

        public static void Audit(string message, System.Diagnostics.TraceEventType eventType)
        {
            Logger.Write(message,
                LOG_AUDIT_CATEGORY,
                1, 2,
                eventType,
                LOG_TITLE);
        }

        public static void Process(string message, System.Diagnostics.TraceEventType eventType)
        {
            Logger.Write(message,
                LOG_PROCESS_CATEGORY,
                1, 2,
                eventType,
                LOG_TITLE);
        }
    }
}
