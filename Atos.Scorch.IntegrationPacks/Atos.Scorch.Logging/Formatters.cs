﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;


namespace JML.ITSM.Logging
{
    public class Formatters
    {
        private const string TEXT_BACK_SLASH = "\\";
        private const string TEXT_CLOSE_BRACKET = ")";
        private const string TEXT_COMMA = ",";
        private const string TEXT_DASH = " - ";
        private const string TEXT_EMPTY_BRACKETS = "()";
        private const string TEXT_EQUALS = ":=";
        private const string TEXT_OPEN_BRACKET = "(";
        private const string TEXT_OPERATOR = ".";
        private const string TEXT_PREFIX = "Call: ";

        public static string FormatAuditLogonMessage(string domain, string login, string prefix)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(prefix);
            sb.Append(domain.Trim());
            sb.Append(TEXT_BACK_SLASH);
            sb.Append(login.Trim());

            return sb.ToString();
        }

        public static string FormatAuditLogonMessage(string domain, string login, string prefix, string message)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(FormatAuditLogonMessage(domain, login, prefix));
            sb.Append(TEXT_DASH);
            sb.Append(message);

            return sb.ToString();
        }

        public static string FormatMessage(string className,  string functionName = "")
        {
            return FormatHeader(className, functionName) + TEXT_EMPTY_BRACKETS;
        }

        public static string FormatMessage(IList<KeyValuePair<string, string>> args, string className,string functionName = "")
        {
            bool isFirstArg = true;
            StringBuilder sb = new StringBuilder(FormatHeader(className, functionName));
             
            sb.Append(TEXT_OPEN_BRACKET);

            foreach (KeyValuePair<string, string> arg in args)
            {
                if (isFirstArg)
                {
                    isFirstArg = false;
                }
                else
                {
                    sb.Append(TEXT_COMMA);
                }

                sb.Append(arg.Key);
                sb.Append(TEXT_EQUALS);
                sb.Append(arg.Value);
            }

            sb.Append(TEXT_CLOSE_BRACKET);

            return sb.ToString();
        }

        private static string FormatHeader(string className, string functionName)
        {
            return TEXT_PREFIX + className + TEXT_OPERATOR + functionName;
        }
    }
}
