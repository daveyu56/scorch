namespace Atos.Scorch.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Collections.Generic;
    using System.Reflection;

    public class AlertModel : DbContext, IDisposedTracker
    {
        public bool IsDisposed { get; set; }

        public virtual DbSet<Alert> Alerts { get; set; }
        public virtual DbSet<ITSMRequestMessage> ITSMRequestMessages { get; set; }
        public virtual DbSet<ITSMResponseMessage> ITSMResponseMessages { get; set; }
        public virtual DbSet<TicketStatu> TicketStatus { get; set; }
        public virtual DbSet<Settings> Settings { get; set; }
        public virtual DbSet<ITSMTemplateMapping> ITSMTemplateMappings { get; set; }
        public virtual DbSet<FileSystems> FileSystems { get; set; }
        public virtual DbSet<SQLServers> SQLServers { get; set; }
        public virtual DbSet<BankHoliday> BankHolidays { get; set; }
        public virtual DbSet<OOHMP> OOHMPs { get; set; }
        public virtual DbSet<CriticalMP> CriticalMPs { get; set; }
        public virtual DbSet<ProductionServers> ProductionServers { get; set; }
        public virtual DbSet<ExchangeServer> ExchangeServers { get; set; }


        public AlertModel() :  base("AlertModel") 
        {
           //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<AlertModel>());
           Database.SetInitializer(new NullDatabaseInitializer<AlertModel>());
          
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());


        }

        protected override void Dispose(bool disposing)
        {
            IsDisposed = true;
            base.Dispose(disposing);
        }

        
  

       
       
    }
}
