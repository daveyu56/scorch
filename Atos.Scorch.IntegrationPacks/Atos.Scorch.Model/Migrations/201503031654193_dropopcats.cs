namespace Atos.Scorch.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dropopcats : DbMigration
    {
        public override void Up()
        {
            //DropTable("dbo.OpCats");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.OpCats",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Category = c.String(maxLength: 50),
                        Type = c.String(maxLength: 50),
                        Item = c.String(maxLength: 50),
                        Value = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.id);
            
        }
    }
}
