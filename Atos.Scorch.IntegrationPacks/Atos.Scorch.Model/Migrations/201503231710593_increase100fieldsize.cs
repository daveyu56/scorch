namespace Atos.Scorch.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class increase100fieldsize : DbMigration
    {
        public override void Up()
        {
            //AlterColumn("dbo.Alert", "Domain", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "Name", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "NetbiosComputerName", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "NetbiosDomainName", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "Owner", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "PrincipalName", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "ResolvedBy", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "SiteName", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "Added_By", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "Last_Modified_By", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "Category", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "Connection", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "ConnectorStatus", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "Context", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "CustomField1", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "CustomField2", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "CustomField3", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "CustomField4", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "CustomField5", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "CustomField6", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "CustomField7", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "CustomField8", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "CustomField9", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "CustomField10", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "ManagementGroup", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "ManagementPackDisplayName", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "ManagementPackFriendlyName", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "ManagementPackMonitorName", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "ManagementPackName", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "ManagementPackVersion", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "MonitoringObjectDisplayName", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "MonitoringObjectHealthState", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "MonitoringObjectName", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "MonitoringObjectPath", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "MonitoringRuleName", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "Parameters", c => c.String());
            //AlterColumn("dbo.Alert", "Server", c => c.String(maxLength: 255));
            //AlterColumn("dbo.Alert", "Username", c => c.String(maxLength: 255));
            //AlterColumn("dbo.ITSMRequestMessage", "AlertName", c => c.String(maxLength: 255));
            //AlterColumn("dbo.ITSMRequestMessage", "AlertPath", c => c.String(maxLength: 255));
            //AlterColumn("dbo.ITSMRequestMessage", "AlertSource", c => c.String(maxLength: 255));
            //AlterColumn("dbo.ITSMRequestMessage", "ManagementPack", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ITSMRequestMessage", "ManagementPack", c => c.String(maxLength: 100));
            AlterColumn("dbo.ITSMRequestMessage", "AlertSource", c => c.String(maxLength: 100));
            AlterColumn("dbo.ITSMRequestMessage", "AlertPath", c => c.String(maxLength: 100));
            AlterColumn("dbo.ITSMRequestMessage", "AlertName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "Username", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "Server", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "Parameters", c => c.String(maxLength: 255));
            AlterColumn("dbo.Alert", "MonitoringRuleName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "MonitoringObjectPath", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "MonitoringObjectName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "MonitoringObjectHealthState", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "MonitoringObjectDisplayName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "ManagementPackVersion", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "ManagementPackName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "ManagementPackMonitorName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "ManagementPackFriendlyName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "ManagementPackDisplayName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "ManagementGroup", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "CustomField10", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "CustomField9", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "CustomField8", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "CustomField7", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "CustomField6", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "CustomField5", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "CustomField4", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "CustomField3", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "CustomField2", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "CustomField1", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "Context", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "ConnectorStatus", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "Connection", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "Category", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "Last_Modified_By", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "Added_By", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "SiteName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "ResolvedBy", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "PrincipalName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "Owner", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "NetbiosDomainName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "NetbiosComputerName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "Name", c => c.String(maxLength: 100));
            AlterColumn("dbo.Alert", "Domain", c => c.String(maxLength: 100));
        }
    }
}
