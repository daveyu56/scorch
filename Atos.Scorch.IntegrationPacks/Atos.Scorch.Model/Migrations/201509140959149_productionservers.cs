namespace Atos.Scorch.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class productionservers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CriticalMP",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MPName = c.String(maxLength: 50),
                        BusinessRule = c.String(),
                        CriticalMPName = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductionServers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ProductionServers");
            DropTable("dbo.CriticalMP");
        }
    }
}
