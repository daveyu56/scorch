namespace Atos.Scorch.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.Alert",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            AlertId = c.Guid(nullable: false),
            //            Description = c.String(),
            //            Domain = c.String(maxLength: 100),
            //            Name = c.String(maxLength: 100),
            //            NetbiosComputerName = c.String(maxLength: 100),
            //            NetbiosDomainName = c.String(maxLength: 100),
            //            Owner = c.String(maxLength: 100),
            //            PrincipalName = c.String(maxLength: 100),
            //            Priority = c.String(maxLength: 50),
            //            ResolvedBy = c.String(maxLength: 100),
            //            Severity = c.String(maxLength: 50),
            //            SiteName = c.String(maxLength: 100),
            //            TimeAlertAdded = c.DateTime(),
            //            TimeAlertResolved = c.DateTime(),
            //            TimeAlertRaised = c.DateTime(),
            //            Date_Added = c.DateTime(),
            //            Added_By = c.String(maxLength: 100),
            //            Date_Last_Modified = c.DateTime(),
            //            Last_Modified_By = c.String(maxLength: 100),
            //            AlertCount = c.Int(nullable: false),
            //            Category = c.String(maxLength: 100),
            //            ClassId = c.Guid(nullable: false),
            //            Connection = c.String(maxLength: 100),
            //            CompanyKnowledge = c.String(),
            //            ProductKnowledge = c.String(),
            //            ConnectorId = c.Guid(nullable: false),
            //            ConnectorStatus = c.String(maxLength: 100),
            //            Context = c.String(maxLength: 100),
            //            CustomField1 = c.String(maxLength: 100),
            //            CustomField2 = c.String(maxLength: 100),
            //            CustomField3 = c.String(maxLength: 100),
            //            CustomField4 = c.String(maxLength: 100),
            //            CustomField5 = c.String(maxLength: 100),
            //            CustomField6 = c.String(maxLength: 100),
            //            CustomField7 = c.String(maxLength: 100),
            //            CustomField8 = c.String(maxLength: 100),
            //            CustomField9 = c.String(maxLength: 100),
            //            CustomField10 = c.String(maxLength: 100),
            //            IsMonitorAlert = c.Boolean(nullable: false),
            //            LastModifiedByNonConnector = c.Boolean(nullable: false),
            //            MaintenanceModeLastModified = c.DateTime(),
            //            ManagementGroup = c.String(maxLength: 100),
            //            ManagementGroupId = c.Guid(nullable: false),
            //            ManagementPackDisplayName = c.String(maxLength: 100),
            //            ManagementPackFriendlyName = c.String(maxLength: 100),
            //            ManagementPackId = c.Guid(nullable: false),
            //            ManagementPackIsSealed = c.Boolean(nullable: false),
            //            ManagementPackMonitorName = c.String(maxLength: 100),
            //            ManagementPackName = c.String(maxLength: 100),
            //            ManagementPackVersion = c.String(maxLength: 100),
            //            MonitoringClassId = c.Guid(nullable: false),
            //            MonitoringObjectDisplayName = c.String(maxLength: 100),
            //            MonitoringObjectFullName = c.String(maxLength: 200),
            //            MonitoringObjectHealthState = c.String(maxLength: 100),
            //            MonitoringObjectId = c.Guid(nullable: false),
            //            MonitoringObjectInMaintenanceMode = c.Boolean(nullable: false),
            //            MonitoringObjectName = c.String(maxLength: 100),
            //            MonitoringObjectPath = c.String(maxLength: 100),
            //            MonitoringRuleId = c.Guid(nullable: false),
            //            MonitoringRuleName = c.String(maxLength: 100),
            //            Parameters = c.String(maxLength: 255),
            //            ProblemId = c.Guid(nullable: false),
            //            RepeatCount = c.Int(nullable: false),
            //            ResolutionState = c.String(maxLength: 50),
            //            RuleId = c.Guid(nullable: false),
            //            Server = c.String(maxLength: 100),
            //            StateLastModified = c.DateTime(),
            //            TimeResolutionStateLastModified = c.DateTime(),
            //            Username = c.String(maxLength: 100),
            //            MessageId = c.Guid(nullable: false),
            //            Status = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.ITSMRequestMessage",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            AlertId = c.Guid(nullable: false),
            //            AlertName = c.String(maxLength: 100),
            //            AlertPath = c.String(maxLength: 100),
            //            AlertSource = c.String(maxLength: 100),
            //            AlertDescription = c.String(),
            //            AlertCreated = c.DateTime(),
            //            ManagementPack = c.String(maxLength: 100),
            //            MessageId = c.Guid(nullable: false),
            //            Date_Added = c.DateTime(),
            //            Added_By = c.String(maxLength: 50),
            //            Alert_Id = c.Long(),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Alert", t => t.Alert_Id)
            //    .Index(t => t.Alert_Id);
            
            //CreateTable(
            //    "dbo.ITSMResponseMessage",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            ResultCode = c.String(maxLength: 100),
            //            ResultMessage = c.String(),
            //            Date_Added = c.DateTime(),
            //            Added_By = c.String(maxLength: 50),
            //            MessageId = c.Guid(nullable: false),
            //            ITSMRequestMessage_Id = c.Long(),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.ITSMRequestMessage", t => t.ITSMRequestMessage_Id)
            //    .Index(t => t.ITSMRequestMessage_Id);
            
            //CreateTable(
            //    "dbo.ITSMTemplateMapping",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            ManagementPack = c.String(maxLength: 50),
            //            Template = c.String(maxLength: 50),
            //            AssignToGroup = c.String(maxLength: 50),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.OpCats",
            //    c => new
            //        {
            //            id = c.Int(nullable: false, identity: true),
            //            Category = c.String(maxLength: 50),
            //            Type = c.String(maxLength: 50),
            //            Item = c.String(maxLength: 50),
            //            Value = c.String(maxLength: 50),
            //        })
            //    .PrimaryKey(t => t.id);
            
            //CreateTable(
            //    "dbo.Settings",
            //    c => new
            //        {
            //            id = c.Int(nullable: false, identity: true),
            //            SettingName = c.String(maxLength: 100),
            //            SettingValue = c.String(),
            //        })
            //    .PrimaryKey(t => t.id);
            
            //CreateTable(
            //    "dbo.TicketStatus",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Status = c.String(maxLength: 50),
            //        })
            //    .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ITSMRequestMessage", "Alert_Id", "dbo.Alert");
            DropForeignKey("dbo.ITSMResponseMessage", "ITSMRequestMessage_Id", "dbo.ITSMRequestMessage");
            DropIndex("dbo.ITSMResponseMessage", new[] { "ITSMRequestMessage_Id" });
            DropIndex("dbo.ITSMRequestMessage", new[] { "Alert_Id" });
            DropTable("dbo.TicketStatus");
            DropTable("dbo.Settings");
            DropTable("dbo.OpCats");
            DropTable("dbo.ITSMTemplateMapping");
            DropTable("dbo.ITSMResponseMessage");
            DropTable("dbo.ITSMRequestMessage");
            DropTable("dbo.Alert");
        }
    }
}
