// <auto-generated />
namespace Atos.Scorch.Model.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.2-31219")]
    public sealed partial class lengthofmodel : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(lengthofmodel));
        
        string IMigrationMetadata.Id
        {
            get { return "201503241541571_lengthofmodel"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
