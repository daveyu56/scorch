namespace Atos.Scorch.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedpriority : DbMigration
    {
        public override void Up()
        {
            //AddColumn("dbo.ITSMRequestMessage", "Priority", c => c.String());
            //AddColumn("dbo.ITSMRequestMessage", "Severity", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ITSMRequestMessage", "Severity");
            DropColumn("dbo.ITSMRequestMessage", "Priority");
        }
    }
}
