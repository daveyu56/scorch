namespace Atos.Scorch.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changelastmodified : DbMigration
    {
        public override void Up()
        {
            //AlterColumn("dbo.Alert", "LastModifiedByNonConnector", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Alert", "LastModifiedByNonConnector", c => c.Boolean(nullable: false));
        }
    }
}
