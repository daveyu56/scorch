namespace Atos.Scorch.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lengthofmodel2 : DbMigration
    {
        public override void Up()
        {
            //AlterColumn("dbo.ITSMRequestMessage", "AlertPath", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ITSMRequestMessage", "AlertPath", c => c.String(maxLength: 255));
        }
    }
}
