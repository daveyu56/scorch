namespace Atos.Scorch.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lengthofmodel : DbMigration
    {
        public override void Up()
        {
        //    AlterColumn("dbo.Alert", "MonitoringObjectFullName", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Alert", "MonitoringObjectFullName", c => c.String(maxLength: 200));
        }
    }
}
