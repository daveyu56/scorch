namespace Atos.Scorch.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixedtemplatemappings : DbMigration
    {
        public override void Up()
        {
            //AddColumn("dbo.ITSMTemplateMapping", "ManagementPackFriendlyName", c => c.String(maxLength: 256));
            //AlterColumn("dbo.ITSMTemplateMapping", "ManagementPack", c => c.String(maxLength: 256));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ITSMTemplateMapping", "ManagementPack", c => c.String(maxLength: 50));
            DropColumn("dbo.ITSMTemplateMapping", "ManagementPackFriendlyName");
        }
    }
}
