namespace Atos.Scorch.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Text;
     
    public partial class ITSMResponseMessage
    {
       
        public long Id { get; set; }
     
        public string ResultCode { get; set; }

        public string ResultMessage { get; set; }

        public DateTime? Date_Added { get; set; }

        public string Added_By { get; set; }

        public Guid MessageId { get; set; }

        public virtual ITSMRequestMessage ITSMRequestMessage { get; set; }
                    

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(string.Format("ResponseId: {0} ", Id.ToString()));

            sb.Append(string.Format("ResultCode: {0} ", ResultCode));

            sb.Append(string.Format("ResultMessage: {0} ", ResultMessage));

            sb.Append(string.Format("Date_Added: {0} ", Date_Added == null ? string.Empty : Date_Added.ToString()));

            sb.Append(string.Format("Added_By: {0} ", Added_By));

            sb.Append(string.Format("MessageIdy: {0} ", MessageId.ToString()));

            string s = sb.ToString();
            return s.Substring(0, s.Length > 1500 ? 1500 : s.Length - 1);
        }
    }


}
