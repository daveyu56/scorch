﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atos.Scorch.Model
{
    public partial class OOHMP
    {
        public int Id { get; set; }

        public string MPName { get; set; }

        public string OOHMPName { get; set; }
    }
}
