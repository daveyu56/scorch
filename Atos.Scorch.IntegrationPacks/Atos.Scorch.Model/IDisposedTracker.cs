﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atos.Scorch.Model
{
    public interface IDisposedTracker
    {
        bool IsDisposed { get; set; }
    }
}
