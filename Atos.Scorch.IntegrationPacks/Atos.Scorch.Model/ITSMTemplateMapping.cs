﻿namespace Atos.Scorch.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;


    public partial class ITSMTemplateMapping
    {

        public int Id { get; set; }

        public string ManagementPack { get; set; }

        public string ManagementPackFriendlyName { get; set; }

        public string Template { get; set; }

        public string AssignToGroup { get; set; }
    }
}
