﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace Atos.Scorch.Model
{
   
    public partial class Settings
    {
      
        public int id { get; set; }

        public string SettingName { get; set; }

        public string SettingValue { get; set; }

      
    }
}
