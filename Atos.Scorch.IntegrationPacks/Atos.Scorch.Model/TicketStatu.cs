namespace Atos.Scorch.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

 
    public partial class TicketStatu
    {
      
        public int Id { get; set; }

        public int StatusId { get; set; }

        public string Status { get; set; }
    }
}
