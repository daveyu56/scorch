﻿using System;
namespace Atos.Scorch.Model.Repositorys
{
    public interface ISettingRepository
    {
        string GetSettingValue(string settingName, string settingType);
    }
}
