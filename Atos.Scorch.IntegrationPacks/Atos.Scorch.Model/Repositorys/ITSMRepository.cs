﻿using Atos.Scorch.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atos.Scorch.Logging;
using Microsoft.Web.Services2.Security.X509;
using Atos.Scorch.WebService.ITSMWebRef;
using System.Web.Services.Protocols;
using System.Net;
using System.Data.Entity.Validation;
using System.IO;

namespace Atos.Scorch.Model.Repositorys
{
    public class ITSMRepository : RepositoryBase<AlertModel>, IITSMRepository
    {
        private ISettingRepository _settings;

        private readonly string CERTIFICATE_NAME = "CertificateName";
        private readonly string PROXY_URL = "ProxyURL";
        private readonly string MAX_POST_COUNT = "MaxPostCount";
        private readonly string UNIX_MP_NAME = "RHEL";
        private readonly string SQL_MP_NAME = "SQL";
        private readonly string SISL_IT_BASIS_MP_NAME = "SISL_IT_BASIS";
        private readonly string CRITICAL_SEVERITY = "Critical";
        private readonly string HEARTBEAT_FAILED = "heartbeat failed";
        private readonly string EXCHANGE_MP_NAME = "EXCHANGE";
        

        public ITSMRepository()
        {
            _settings = new SettingRepository();
        }

        public OperationStatus PostAlertToITSM(SCOMAlert alert, string template)
        {
            bool status = false;

            ITSMRequestMessage request;
               
            string settingType = string.Empty;
            int maxTries = 0;
            bool newAlert = true;
                
            if (!int.TryParse(_settings.GetSettingValue(MAX_POST_COUNT, settingType), out maxTries))
                maxTries = 3;

            int tryCount = 0;
            string resultMessage = string.Empty;

            
            using (DataContext)
            {

                Alert thisAlert;

                if (alert.Status == 0)
                {
                    thisAlert = Alert.CopyAlert(alert);
                    newAlert = true;                    
                }
                else
                {
                    thisAlert = DataContext.Alerts.Where(p => p.AlertId == alert.AlertId).FirstOrDefault();
                    CopyToSaved(alert, thisAlert);
                    newAlert = false;
                }


                try
                {

                    var service = new FUS_HPDInterfaceStagingService();

                    request = CreateITSMRequestFromAlert(thisAlert, template);

                    Log.Info(string.Format("Sending Request to ITSM : {0}", request.ToString()));                                 

                    SetupService(request, service);

                    string resCode = string.Empty;
                    string resMessage = string.Empty;

                    while (tryCount < maxTries)
                    {
                        tryCount++;

                        try
                        {
                           
                            
                            string path = request.AlertPath.Length > 999 ? request.AlertPath.Substring(0, 999) : request.AlertPath.Substring(0, request.AlertPath.Length );
                            string description = request.AlertDescription.Length > 999 ? request.AlertDescription.Substring(0, 999) : request.AlertDescription.Substring(0, request.AlertDescription.Length);
                            resCode = service.Create(request.MessageId.ToString(), request.AlertId.ToString(), request.AlertName, path, request.AlertSource, description,
                                               request.AlertCreated.ToString(), request.Template, out resMessage);
                        }
                        catch (Exception e)
                        {
                            resCode = "1";
                            resMessage = e.Message;
                            Log.Exception("Error from service.Create", e);
                            if (e.InnerException != null)
                                Log.Exception("Error from service.Create - inner exception", e.InnerException);
                               
                        }

                            ITSMResponseMessage response = CreateITSMResponseFromReply(resCode, resMessage, request);

                        Log.Info(string.Format("Received Response from ITSM : {0}", response.ToString()));

                        request.ITSMResponseMessages.Add(response);

                          
                        if (resCode == "0")
                        {
                            thisAlert.Status = 1;
                            break;
                        }

                    }

                    thisAlert.ITSMRequestMessages.Add(request);

                    if (newAlert)
                        DataContext.Alerts.Add(thisAlert);

                    try
                    {
                        DataContext.SaveChanges();
                    }
                    catch (DbEntityValidationException ex)
                    {
                        StringBuilder sb = new StringBuilder();

                        foreach (var failure in ex.EntityValidationErrors)
                        {
                            sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                            foreach (var error in failure.ValidationErrors)
                            {
                                sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                                sb.AppendLine();
                            }
                        }

                        Log.Exception("Error in SaveChanges() - " + sb.ToString(), ex);
                    }


                    status = (resCode == "0") ? true : false;

                    return new OperationStatus() { Status = status, Message = resMessage };

                }
                catch (EntityException ee)
                {
                    Log.Exception(ee);
                    return OperationStatus.CreateFromException("Error saving alert to database", ee);
                }
                catch (Exception e)
                {
                    Log.Exception(e);
                    return OperationStatus.CreateFromException("Error occurred in PostAlertToITSM()", e);
                }

            }
        }

        private void CopyToSaved(SCOMAlert alert, Alert thisAlert)
        {
            thisAlert.Description = alert.AlertDescription;
            thisAlert.AlertId = alert.AlertId;
            thisAlert.Domain = alert.Domain;
            thisAlert.Name = alert.Name;
            thisAlert.NetbiosComputerName = alert.NetbiosComputerName;
            thisAlert.NetbiosDomainName = alert.NetbiosDomainName;
            thisAlert.Owner = alert.Owner;
            thisAlert.PrincipalName = alert.PrincipalName;
            thisAlert.Priority = alert.Priority;
            thisAlert.ResolvedBy = alert.ResolvedBy;
            thisAlert.Severity = alert.Severity;
            thisAlert.SiteName = alert.SiteName;
            thisAlert.TimeAlertAdded = alert.TimeAdded;
            thisAlert.TimeAlertRaised = alert.TimeRaised;
            thisAlert.TimeAlertResolved = alert.TimeResolved;
            thisAlert.Date_Added = DateTime.Now;
            thisAlert.Added_By = "SCOM Alert Web Api";
            thisAlert.Date_Last_Modified = null;
            thisAlert.Last_Modified_By = string.Empty;

            // added all published data
            thisAlert.AlertCount = alert.AlertCount;
            thisAlert.Category = alert.Category;
            thisAlert.ClassId = alert.ClassId;
            thisAlert.Connection = alert.Connection;
            thisAlert.CompanyKnowledge = alert.CompanyKnowledge;

            thisAlert.ProductKnowledge = alert.ProductKnowledge;
            thisAlert.ConnectorId = alert.ConnectorId;
            thisAlert.ConnectorStatus = alert.ConnectorStatus;
            thisAlert.Context = alert.Context;
            thisAlert.CustomField1 = alert.CustomField1;
            thisAlert.CustomField2 = alert.CustomField2;

            thisAlert.CustomField3 = alert.CustomField3;
            thisAlert.CustomField4 = alert.CustomField4;
            thisAlert.CustomField5 = alert.CustomField5;
            thisAlert.CustomField6 = alert.CustomField6;
            thisAlert.CustomField7 = alert.CustomField7;
            thisAlert.CustomField8 = alert.CustomField8;
            thisAlert.CustomField9 = alert.CustomField9;
            thisAlert.CustomField10 = alert.CustomField10;
            thisAlert.IsMonitorAlert = alert.IsMonitorAlert;

            thisAlert.LastModifiedByNonConnector = alert.LastModifiedByNonConnector;

            thisAlert.MaintenanceModeLastModified = alert.MaintenanceModeLastModified;

            thisAlert.ManagementGroup = alert.ManagementGroup;

            thisAlert.ManagementGroupId = alert.ManagementGroupId;

            thisAlert.ManagementPackDisplayName = alert.ManagementPackDisplayName;

            thisAlert.ManagementPackFriendlyName = alert.ManagementPackFriendlyName;

            thisAlert.ManagementPackId = alert.ManagementPackId;

            thisAlert.ManagementPackIsSealed = alert.ManagementPackIsSealed;

            thisAlert.ManagementPackMonitorName = alert.ManagementPackMonitorName;

            thisAlert.ManagementPackName = alert.ManagementPackName;

            thisAlert.ManagementPackVersion = alert.ManagementPackVersion;

            thisAlert.MonitoringClassId = alert.MonitoringClassId;

            thisAlert.MonitoringObjectDisplayName = alert.MonitoringObjectDisplayName;

            thisAlert.MonitoringObjectFullName = alert.MonitoringObjectFullName;

            thisAlert.MonitoringObjectHealthState = alert.MonitoringObjectHealthState;

            thisAlert.MonitoringObjectId = alert.MonitoringObjectId;

            thisAlert.MonitoringObjectInMaintenanceMode = alert.MonitoringObjectInMaintenanceMode;

            thisAlert.MonitoringObjectName = alert.MonitoringObjectName;

            thisAlert.MonitoringObjectPath = alert.MonitoringObjectPath;

            thisAlert.MonitoringRuleId = alert.MonitoringRuleId;

            thisAlert.MonitoringRuleName = alert.MonitoringRuleName;

            thisAlert.Parameters = alert.Parameters;

            thisAlert.ProblemId = alert.ProblemId;

            thisAlert.RepeatCount = alert.RepeatCount;

            thisAlert.ResolutionState = alert.ResolutionState;

            thisAlert.RuleId = alert.RuleId;

            thisAlert.Server = alert.Server;

            thisAlert.StateLastModified = alert.StateLastModified;

            thisAlert.TimeResolutionStateLastModified = alert.TimeResolutionStateLastModified;

            thisAlert.Username = alert.Username;

            thisAlert.MessageId = alert.MessageId;

            thisAlert.Status = alert.Status;
        }

        private ITSMResponseMessage CreateITSMResponseFromReply(string resCode, string resMessage, ITSMRequestMessage request)
        {
            ITSMResponseMessage response = new ITSMResponseMessage();
            response.ResultCode = resCode;
            response.ResultMessage = resMessage;
            response.Added_By = this.GetType().ToString();
            response.Date_Added = DateTime.Now;
            response.MessageId = request.MessageId;
           
            return response;
        }

        private ITSMRequestMessage CreateITSMRequestFromAlert(Atos.Scorch.Model.Alert alert, string template)
        {
            
            ITSMRequestMessage request = new ITSMRequestMessage();

            request.MessageId = alert.MessageId;
            request.AlertId = alert.AlertId;
            request.AlertName = alert.Name;
            request.AlertPath = string.Format ("{0}\\{1}", alert.MonitoringObjectPath, alert.MonitoringObjectDisplayName);
            request.AlertSource = alert.MonitoringObjectDisplayName;
            request.AlertDescription = alert.Description.Replace("\"", "");
            request.AlertCreated = alert.TimeAlertAdded;
            request.ManagementPack = alert.ManagementPackName;
            request.Added_By = this.GetType().ToString();
            request.Date_Added = DateTime.Now;
            request.Severity = alert.Severity;
            request.Priority = alert.Priority;
            request.Template = template;

            return request;
        }

        public Alert AlertAlreadyPosted(Guid alertId)
        {
            if (alertId == null)
                return null;

            Alert alert = null;

           
            using (DataContext)
            {

                try
                {
                    alert = DataContext.Alerts.Where(p => p.AlertId == alertId).FirstOrDefault();
                }
                catch (Exception e)
                {
                    string s = "here";
                }

            }

            return alert;

        }

        public bool ProcessingManagementPack(string packName)
        {
            bool processing = true;                     
            
            if (string.IsNullOrWhiteSpace(packName))
                return !processing;

            using (DataContext)
            {
                var template = DataContext.ITSMTemplateMappings.Where(p => p.ManagementPack == packName).FirstOrDefault();

                if (template == null)
                    processing = false;

                if (string.IsNullOrWhiteSpace(template.Template))
                    processing = false;

            }

            return processing;
        }

        //public Atos.Scorch.Domain.ITSMTemplateMapping GetITSMTemplateFromAlert(alert.ManagementPackName, alert.MonitoringObjectPath, OOH, alert.Severity, alert.MonitoringObjectName, alert.Name, alert.MonitoringObjectDisplayName);
        public Atos.Scorch.Domain.ITSMTemplateMapping GetITSMTemplateFromAlert(string packName, string source, bool OOH, string severity, string objectName, string alertName, string displayName)
        {

            string alteredPackName = string.Empty;
            string eventText = string.Empty;

            using (DataContext)
            {
               
                 var template = DataContext.ITSMTemplateMappings.Where(p => p.ManagementPack == packName).FirstOrDefault();

                 if (template == null)
                     return new Atos.Scorch.Domain.ITSMTemplateMapping() { Template = "IGNORE", ManagementPack = "IGNORE", AssignToGroup = "IGNORE" };

                 if (template.Template == UNIX_MP_NAME)
                    alteredPackName = CheckForFileSystems(objectName);

                
                 if (!string.IsNullOrWhiteSpace(alteredPackName) && alteredPackName == "IGNORE")
                     return new Atos.Scorch.Domain.ITSMTemplateMapping() { Template = "IGNORE", ManagementPack = "IGNORE", AssignToGroup = "IGNORE" };

                 if (template.Template == SQL_MP_NAME)
                     alteredPackName = CheckSQLServer(source);

                 if (!string.IsNullOrWhiteSpace(alteredPackName))
                     template.Template = alteredPackName;

                 if (severity.Equals(CRITICAL_SEVERITY) && OOH)
                 {
                     if (IgnoreAlertForBusinessRule(source, displayName))
                     {
                         return new Atos.Scorch.Domain.ITSMTemplateMapping() { Template = "IGNORE", ManagementPack = "IGNORE", AssignToGroup = "IGNORE" };
                     }
                     else
                     {
                         template.Template = GetOOHTemplate(template.Template);
                     }
                 }
                 else if (severity.Equals(CRITICAL_SEVERITY) && template.Template != EXCHANGE_MP_NAME && (ProductionServer(objectName) || ProductionServer(displayName)))
                 {
                     template.Template = GetCriticalTemplateForBusinessRule(template.Template, alertName);
                 }
                 else if (template.Template == EXCHANGE_MP_NAME)
                 {
                     if (IgnoreAlertForBusinessRule(source, displayName))
                     {
                         return new Atos.Scorch.Domain.ITSMTemplateMapping() { Template = "IGNORE", ManagementPack = "IGNORE", AssignToGroup = "IGNORE" };
                     }
                     else if (severity.Equals(CRITICAL_SEVERITY))
                     {
                         template.Template = GetCriticalTemplateForBusinessRule(template.Template);
                     }
                 }





                 return new Atos.Scorch.Domain.ITSMTemplateMapping() { Template = template.Template, ManagementPack = template.ManagementPack, AssignToGroup = template.AssignToGroup };
            }

            //return new Atos.Scorch.Domain.ITSMTemplateMapping() { Template = "SCOM WINTEL", ManagementPack = "Windows", AssignToGroup = "Assigned To Engineering" };
        }

        private bool IgnoreAlertForBusinessRule(string source, string eventText)
        {
            using (DataContext)
            {
                var servers = DataContext.ExchangeServers.ToList();

                foreach (var server in servers)
                {
                      if (source.Contains(server.Name) && eventText.Contains(server.Rule))
                      {
                          return true;
                      }
                }
            }

            return false;
        }

        private string GetCriticalTemplateForBusinessRule(string template)
        {
            using (DataContext)
            {
                var criticalTemplates = DataContext.CriticalMPs.Where(p => p.MPName.Equals(template)).ToList();

                if (criticalTemplates == null)
                {
                    return template;
                }
                else
                {
                   var criticalTemplate = criticalTemplates.FirstOrDefault();
                   return criticalTemplate.CriticalMPName;


                }
            
            }
        }

        private bool ProductionServer(string objectName)
        {
            bool prodServer = false;

            if (string.IsNullOrWhiteSpace(objectName))
                return prodServer;

            using (DataContext)
            {
                prodServer = DataContext.ProductionServers.Any(p => p.Name == objectName);
            }

            return prodServer;

        }

        private string GetCriticalTemplateForBusinessRule(string template, string alertName)
        {
            using (DataContext)
            {
                var criticalTemplates = DataContext.CriticalMPs.Where(p => p.MPName.Equals(template)).ToList();

                if (criticalTemplates == null)
                {
                    return template;
                }
                else
                {
                    foreach (var criticalTemplate in criticalTemplates)
                    {
                        if (alertName.ToLower().Contains(criticalTemplate.BusinessRule.ToLower()))
                        {
                            return criticalTemplate.CriticalMPName;
                        }
                     }

                    return template;
                }
            }
        }

        private string GetOOHTemplate(string template)
        {
            var oohTemplate = DataContext.OOHMPs.Where(p => p.MPName == template).FirstOrDefault();

            if (oohTemplate == null)
                return template;

            return oohTemplate.OOHMPName;
        }

        private string CheckSQLServer(string source)
        {
            string alteredPackName = string.Empty;

            var sqlServers = DataContext.SQLServers.ToList();

            foreach (var server in sqlServers)
            {
                if (source.Contains(server.Name))
                {
                    alteredPackName = server.ManagementPack;
                    break;
                }
            }


            return alteredPackName; 
           

        }

        private string CheckForFileSystems(string source)
        {
            string alteredPackName = string.Empty;

            // check if it's really Storage or Avamar or SISL_IT_BASIS
            var fileSystems = DataContext.FileSystems.ToList();

            foreach (var fs in fileSystems)
            {
                if (source.Contains(fs.Name))
                {
                    alteredPackName = fs.Type;
                    break;
                }
            }
          
            return alteredPackName;
        }

        private void SetupService(ITSMRequestMessage request, FUS_HPDInterfaceStagingService _service)
        {
            string settingType = string.Empty;

            string certName = _settings.GetSettingValue(CERTIFICATE_NAME, settingType);
            string proxyUrl = _settings.GetSettingValue(PROXY_URL, settingType);

            X509CertificateStore store = X509CertificateStore.LocalMachineStore(X509CertificateStore.MyStore);

            store.OpenRead();

            X509CertificateCollection col = (X509CertificateCollection)store.FindCertificateBySubjectString(certName);

            X509Certificate cert = col[0];

            _service.ClientCertificates.Add(cert);

            _service.Proxy = new WebProxy(proxyUrl, true);

            _service.UseDefaultCredentials = true;


            _service.Url = _settings.GetSettingValue("ServiceUrl", null);
            _service.Url = "https://itsm-int.it-solutions.atos.net/arsys/services/ARService?server=itsm-int-ar.it-solutions.atos.net&webService=FUS_HPDInterfaceStaging";

            
            _service.AuthenticationInfoValue = new AuthenticationInfo();
            _service.AuthenticationInfoValue.userName = _settings.GetSettingValue("ServiceUserName", null);
            _service.AuthenticationInfoValue.password = _settings.GetSettingValue("ServicePassword", null);
        }

       public bool IsOOH(DateTime? alertAdded)
       {

           if (!alertAdded.HasValue)
               return false;

           TimeSpan startOOH = new TimeSpan(18, 0, 0);
           TimeSpan endOOH = new TimeSpan(08, 0, 0);

           int iStart;
           int iEnd;

           if (!int.TryParse(_settings.GetSettingValue("START_OOH", string.Empty), out iStart))
               iStart = 18;
               
           startOOH = new TimeSpan(iStart, 0, 0);

           if (!int.TryParse(_settings.GetSettingValue("END_OOH", string.Empty), out iEnd))
               iEnd = 8;
               
           endOOH = new TimeSpan(iEnd, 0, 0);

           TimeSpan now = alertAdded.Value.TimeOfDay;

           if (startOOH <= endOOH)
           {
               // start and stop times are in the same day
               if (now >= startOOH && now <= endOOH)
               {
                   // current time is between start and stop
                   return true;
               }
           }
           else
           {
               // start and stop times are in different days
               if (now >= startOOH || now <= endOOH)
               {
                   // current time is between start and stop
                   return true;
               }
           }

           if (IsBankHoliday (alertAdded))
               return true;

           return false;
           
       }

       private bool IsBankHoliday(DateTime? alertAdded)
       {
           using (DataContext)
           {
               var bankHoliday = DataContext.BankHolidays.Where(p => p.Year == alertAdded.Value.Year && p.Month == alertAdded.Value.Month && p.Day == alertAdded.Value.Day).FirstOrDefault();

               if (bankHoliday != null)
                   return true;
               else
                   return false;
           }
               
       }

       protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_settings != null)
                {
                    var disposable = _settings as IDisposable;
                    disposable.Dispose();
                    disposable = null;
                }
            }

            base.Dispose(disposing);
        }

    }
}
