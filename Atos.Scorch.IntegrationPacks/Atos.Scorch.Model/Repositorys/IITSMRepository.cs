﻿using Atos.Scorch.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atos.Scorch.Model.Repositorys
{
    public interface IITSMRepository
    {
        OperationStatus PostAlertToITSM(SCOMAlert alert, string template);

        Alert AlertAlreadyPosted(Guid alertId);

        Atos.Scorch.Domain.ITSMTemplateMapping GetITSMTemplateFromAlert(string packName, string description, bool OOH, string severity, string objectName, string alertName, string displayName);

        bool ProcessingManagementPack(string packName);

        bool IsOOH(DateTime? alertAdded);
    }
}
