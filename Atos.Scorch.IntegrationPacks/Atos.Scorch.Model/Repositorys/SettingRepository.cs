﻿using Atos.Scorch.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atos.Scorch.Model.Repositorys
{

    public class SettingRepository : RepositoryBase<AlertModel>, ISettingRepository
    {
        private static readonly IDictionary<string, string> DefaultSettings = new Dictionary<string, string>()
        {
                {"SCOMBaseAddress", @"http://localhost:8090"},
                {"MaxPostCount", "3"},
                {"ServiceUrl", @"https://itsmuk.it-solutions.atos.net/arsys/services/ARService?server=itsmuk-ar.siemens-it-solutions.com&webService=FUS_HPDInterfaceStaging"},
                {"ServiceUserName", "SCOMADMIN"},
                {"ServicePassword", @"Sc0MAdm!n"},
                {"CertificateName", @"itsmuk.it-solutions.atos.net"},
                {"ProxyURL", @"http://www-cache.reith.bbc.co.uk:80/"},
                {"SISL_IT_BASIS_MP_NAME", "SISL_IT_BASIS"},
                {"START_OOH", "18"},
                {"END_OOH", "8"}
                
        };

        public SettingRepository()
        {

        }

        public string GetSettingValue (string settingName, string settingType)
        {
            if (string.IsNullOrEmpty(settingType))
                settingType = string.Empty;

            if (string.IsNullOrEmpty(settingName))
                settingName = string.Empty;

             using (DataContext)
             {
                 var setting = DataContext.Settings.Where(p => p.SettingName.ToUpper() == settingName.ToUpper()).FirstOrDefault();


                 if (setting != null)
                 {
                     if (!string.IsNullOrWhiteSpace(setting.SettingValue))
                     {
                         return setting.SettingValue;
                     }
                     else
                     {
                         return DefaultSettings[settingName];
                     }
                 }
                 else
                 {
                     return DefaultSettings[settingName];
                 }
             }
          
        }


      


       
    }
}
