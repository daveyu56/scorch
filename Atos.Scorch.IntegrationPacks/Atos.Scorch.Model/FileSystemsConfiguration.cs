﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Atos.Scorch.Model
{
    public class FileSystemsConfiguration : EntityTypeConfiguration<FileSystems>
    {
        public FileSystemsConfiguration()
        {
            this.ToTable("FileSystems");
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(p => p.Name).HasMaxLength(100);
            this.Property(p => p.Type).HasMaxLength(30);

        }
    }
}
