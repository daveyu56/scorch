namespace Atos.Scorch.Model
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Text;

 
    public partial class ITSMRequestMessage
    {

        public ITSMRequestMessage()
        {
            ITSMResponseMessages = new List<ITSMResponseMessage>();
        }

        
        public long Id { get; set; }

        public Guid AlertId { get; set; }
       
        public string AlertName { get; set; }

        public string AlertPath { get; set; }

        public string AlertSource { get; set; }

        public string AlertDescription { get; set; }

        public DateTime? AlertCreated { get; set;}

        public string ManagementPack { get; set; }

        public Guid MessageId {get; set;}

        public DateTime? Date_Added { get; set; }

        public string Added_By { get; set; }

        public string Priority { get; set; }

        public string Severity { get; set; }

        public string Template { get; set; }

        public virtual ICollection<ITSMResponseMessage> ITSMResponseMessages { get; set; }


       
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(string.Format("Id: {0} ", Id.ToString()));

            sb.Append(string.Format("AlertId: {0} ", AlertId.ToString()));

            sb.Append(string.Format("AlertName: {0} ", AlertName));
              
            sb.Append(string.Format("AlertPath: {0} ", AlertPath));

            sb.Append(string.Format("AlertSource : {0} ", AlertSource));

            sb.Append(string.Format("AlertDescription : {0} ", AlertDescription));
       
            sb.Append(string.Format("AlertCreated : {0} ", AlertCreated.ToString()));
        
            sb.Append(string.Format("ManagementPack : {0} ", ManagementPack));

            sb.Append(string.Format("MessageId : {0} ", MessageId.ToString()));

            sb.Append(string.Format("Date_Added: {0} ", Date_Added == null ? string.Empty : Date_Added.ToString()));

            sb.Append(string.Format("Added_By: {0} ", Added_By));

            foreach (var response in ITSMResponseMessages)
            {
                sb.Append(response.ToString());
            }

            string s = sb.ToString();
            return s.Substring(0, s.Length > 1500 ? 1500 : s.Length - 1);
        }
    }


}
