﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Atos.Scorch.Model
{
    public class AlertConfiguration : EntityTypeConfiguration<Alert>
    {
        public AlertConfiguration()
        {
            this.ToTable("Alert");

            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(p => p.Domain).HasMaxLength(255);
            this.Property(p => p.Name).HasMaxLength(255);
            this.Property(p => p.NetbiosComputerName).HasMaxLength(255);
            this.Property(p => p.NetbiosDomainName).HasMaxLength(255);
            this.Property(p => p.Owner).HasMaxLength(255);
            this.Property(p => p.PrincipalName).HasMaxLength(255);
            this.Property(p => p.Priority).HasMaxLength(50);
            this.Property(p => p.ResolvedBy).HasMaxLength(255);
            this.Property(p => p.Severity).HasMaxLength(50);
            this.Property(p => p.SiteName).HasMaxLength(255);
            this.Property(p => p.Added_By).HasMaxLength(255);
            this.Property(p => p.Last_Modified_By).HasMaxLength(255);

            // added all published alert data items
            //this.Property(p => p.AlertCount)

            this.Property(p => p.Category).HasMaxLength(255);

           // this.Property(p => p.ClassId).HasMaxLength(255);
        
            this.Property(p => p.Connection).HasMaxLength(255);

            this.Property(p => p.CompanyKnowledge).HasColumnType("nvarchar(max)");

            this.Property(p => p.ProductKnowledge).HasColumnType("nvarchar(max)");

          //  this.Property(p => p.ConnectorId).HasMaxLength(255);

            this.Property(p => p.ConnectorStatus).HasMaxLength(255);

            this.Property(p => p.Context).HasMaxLength(255);

            this.Property(p => p.CustomField1).HasMaxLength(255);

            this.Property(p => p.CustomField2).HasMaxLength(255);

            this.Property(p => p.CustomField3).HasMaxLength(255);

            this.Property(p => p.CustomField4).HasMaxLength(255);

            this.Property(p => p.CustomField5).HasMaxLength(255);

            this.Property(p => p.CustomField6).HasMaxLength(255);

            this.Property(p => p.CustomField7).HasMaxLength(255);

            this.Property(p => p.CustomField8).HasMaxLength(255);

            this.Property(p => p.CustomField9).HasMaxLength(255);

            this.Property(p => p.CustomField10).HasMaxLength(255);

            //this.Property(p => p.IsMonitorAlert)

            //this.Property(p => p.LastModifiedByNonConnector)

            //this.Property(p => p.MaintenanceModeLastModified)

            this.Property(p => p.ManagementGroup).HasMaxLength(255);

          //  this.Property(p => p.ManagementGroupId).HasMaxLength(255);

           this.Property(p => p.ManagementPackDisplayName).HasMaxLength(255);

            this.Property(p => p.ManagementPackFriendlyName).HasMaxLength(255);

          //  this.Property(p => p.ManagementPackId).HasMaxLength(255);

            //this.Property(p => p.ManagementPackIsSealed)

            this.Property(p => p.ManagementPackMonitorName).HasMaxLength(255);

            this.Property(p => p.ManagementPackName).HasMaxLength(255);

            this.Property(p => p.ManagementPackVersion).HasMaxLength(255);

         //   this.Property(p => p.MonitoringClassId).HasMaxLength(255);

            this.Property(p => p.MonitoringObjectDisplayName).HasMaxLength(255);

            this.Property(p => p.MonitoringObjectFullName).HasColumnType("nvarchar(max)");

            this.Property(p => p.MonitoringObjectHealthState).HasMaxLength(255);

          //  this.Property(p => p.MonitoringObjectId).HasMaxLength(255);

            //this.Property(p => p.MonitoringObjectInMaintenanceMode)

            this.Property(p => p.MonitoringObjectName).HasMaxLength(255);

            this.Property(p => p.MonitoringObjectPath).HasMaxLength(255);

           // this.Property(p => p.MonitoringRuleId).HasMaxLength(255);

            this.Property(p => p.MonitoringRuleName).HasMaxLength(255);

            this.Property(p => p.Parameters).HasColumnType("nvarchar(max)");

         //   this.Property(p => p.ProblemId).HasMaxLength(255);

            //this.Property(p => p.RepeatCount)

            this.Property(p => p.ResolutionState).HasMaxLength(50);

          //  this.Property(p => p.RuleId).HasMaxLength(255);

            this.Property(p => p.Server).HasMaxLength(255);

            //this.Property(p => p.StateLastModified)

            //this.Property(p => p.TimeResolutionStateLastModified)

            this.Property(p => p.Username).HasMaxLength(255);

         //   this.HasOptional(f => f.ITSMRequestMessage).WithRequired(s => s.Alert);

          


        }
    }
}
