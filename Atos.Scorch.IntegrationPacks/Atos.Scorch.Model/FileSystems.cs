﻿namespace Atos.Scorch.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;


    public partial class FileSystems
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
       
    }
}
