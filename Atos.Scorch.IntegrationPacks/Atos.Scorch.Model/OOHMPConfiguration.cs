﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Atos.Scorch.Model
{
    public class OOHMPConfiguration : EntityTypeConfiguration<OOHMP>
    {
        public OOHMPConfiguration()
        {
            this.ToTable("OOHMPS");
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(p => p.MPName).HasMaxLength(50);
            this.Property(p => p.OOHMPName).HasMaxLength(50);

        }
    }
}
