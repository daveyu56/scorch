﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Atos.Scorch.Model
{
    public class SQLServersConfiguration : EntityTypeConfiguration<SQLServers>
    {
        public SQLServersConfiguration()
        {
            this.ToTable("SQLServers");
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(p => p.Name).HasMaxLength(255);
            this.Property(p => p.ManagementPack).HasMaxLength(256);

        }
    }
}
