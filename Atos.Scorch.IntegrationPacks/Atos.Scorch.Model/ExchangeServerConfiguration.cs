﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Atos.Scorch.Model
{
    public class ExchangeServerConfiguration : EntityTypeConfiguration<ExchangeServer>
    {
        public ExchangeServerConfiguration()
        {
            this.ToTable("ExchangeServer");
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(p => p.Name).HasMaxLength(255);
            this.Property(p => p.Rule).HasMaxLength(50);

        }
    }
}
