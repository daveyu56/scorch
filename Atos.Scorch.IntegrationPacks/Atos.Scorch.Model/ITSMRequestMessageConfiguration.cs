﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Atos.Scorch.Model
{
    public class ITSMRequestMessageConfiguration : EntityTypeConfiguration<ITSMRequestMessage>
    {
        public ITSMRequestMessageConfiguration()
        {
            this.ToTable("ITSMRequestMessage");
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(p => p.AlertName).HasMaxLength(255);

            this.Property(p => p.AlertPath).HasColumnType("nvarchar(max)");

            this.Property(p => p.AlertSource).HasMaxLength(255);

            this.Property(p => p.AlertDescription).HasColumnType("nvarchar(max)");

            this.Property(p => p.ManagementPack).HasMaxLength(255);

             this.Property(p => p.Added_By).HasMaxLength(50);

             this.Property(p => p.Priority).HasMaxLength(30);

             this.Property(p => p.Severity).HasMaxLength(30);

             this.Property(p => p.Template).HasMaxLength(50);

            //this.HasMany<ITSMResponseMessage>(s => s.ITSMResponseMessages).WithRequired(s => s.ITSMRequestMessage);          
        }
    }
}
