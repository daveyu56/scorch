namespace Atos.Scorch.Model
{
    using Atos.Scorch.Domain;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

   
    public partial class Alert
    {

        public Alert()
        {

            ITSMRequestMessages = new List<ITSMRequestMessage>();
        }

        private string _description;
        private const int MAX_DESC_LENGTH = 5000;
        
        public long Id { get; set; }

        public Guid AlertId { get; set; }
      
        public string Description { 
            get { return _description.Length > MAX_DESC_LENGTH ?  _description.Substring(0, MAX_DESC_LENGTH) : _description; }
            set { _description = value; }
        }

        public string Domain { get; set; }

        public string Name { get; set; }

        public string NetbiosComputerName { get; set; }

        public string NetbiosDomainName { get; set; }

        public string Owner { get; set; }

        public string PrincipalName { get; set; }

        public string Priority { get; set; }

        public string ResolvedBy { get; set; }

        public string Severity { get; set; }

        public string SiteName { get; set; }

        public DateTime? TimeAlertAdded { get; set; }

        public DateTime? TimeAlertResolved { get; set; }

        public DateTime? TimeAlertRaised { get; set; }

        public DateTime? Date_Added { get; set; }

        public string Added_By { get; set; }

        public DateTime? Date_Last_Modified { get; set; }

        public string Last_Modified_By { get; set; }

      

        // added all published data columns
        public int AlertCount { get; set; }

        public string Category { get; set; }

        public Guid ClassId { get; set; }
        
        public string Connection { get; set; }

        public string CompanyKnowledge {get; set; }

        public string ProductKnowledge { get; set; }

        public Guid ConnectorId { get; set; }

        public string ConnectorStatus { get; set; }

        public string Context { get; set; }

        public string CustomField1 { get; set; }

        public string CustomField2 { get; set; }

        public string CustomField3 { get; set; }

        public string CustomField4 { get; set; }

        public string CustomField5 { get; set; }

        public string CustomField6 { get; set; }

        public string CustomField7 { get; set; }

        public string CustomField8 { get; set; }

        public string CustomField9 { get; set; }

        public string CustomField10 { get; set; }

        public bool IsMonitorAlert { get; set; }

        public DateTime? LastModifiedByNonConnector { get; set; }

        public DateTime? MaintenanceModeLastModified { get; set; }

        public string ManagementGroup { get; set; }

        public Guid ManagementGroupId { get; set; }

        public string ManagementPackDisplayName { get; set; }

        public string ManagementPackFriendlyName { get; set; }

        public Guid ManagementPackId { get; set; }

        public bool ManagementPackIsSealed { get; set; }

        public string ManagementPackMonitorName { get; set; }

        public string ManagementPackName { get; set; }

        public string ManagementPackVersion { get; set; }

        public Guid MonitoringClassId { get; set; }

        public string MonitoringObjectDisplayName { get; set; }

        public string  MonitoringObjectFullName { get; set; }

        public string MonitoringObjectHealthState { get; set; }

        public Guid MonitoringObjectId { get; set; }

        public bool MonitoringObjectInMaintenanceMode { get; set; }

        public string MonitoringObjectName { get; set; }

        public string MonitoringObjectPath { get; set; }

        public Guid MonitoringRuleId { get; set; }

        public string MonitoringRuleName { get; set; }

        public string Parameters { get; set; }

        public Guid ProblemId { get; set; }

        public int RepeatCount { get; set; }

        public string ResolutionState { get; set; }

        public Guid RuleId { get; set; }

        public string Server { get; set; }

        public DateTime? StateLastModified { get; set; }

        public DateTime? TimeResolutionStateLastModified { get; set; }

        public string Username { get; set; }

        public Guid MessageId { get; set; }

        public int Status { get; set; }

        public virtual ICollection<ITSMRequestMessage> ITSMRequestMessages { get; set; }

              

     
      
    }
}
