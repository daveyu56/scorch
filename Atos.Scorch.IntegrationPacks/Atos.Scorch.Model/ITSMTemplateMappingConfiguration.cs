﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Atos.Scorch.Model
{
    public class ITSMTemplateMappingConfiguration : EntityTypeConfiguration<ITSMTemplateMapping>
    {
        public ITSMTemplateMappingConfiguration()
        {
            this.ToTable("ITSMTemplateMapping");
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(p => p.ManagementPack).HasMaxLength(256);
            this.Property(p => p.ManagementPackFriendlyName).HasMaxLength(256);
            this.Property(p => p.Template).HasMaxLength(50);
            this.Property(p => p.AssignToGroup).HasMaxLength(50);
        }
         
    }
}

