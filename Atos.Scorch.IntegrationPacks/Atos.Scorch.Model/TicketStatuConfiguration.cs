﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Atos.Scorch.Model
{
    public class TicketStatuConfiguration : EntityTypeConfiguration<TicketStatu>
    {
        public TicketStatuConfiguration()
        {
            this.ToTable("TicketStatus");
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(p => p.Status).HasMaxLength(50);
        }
    }
}
