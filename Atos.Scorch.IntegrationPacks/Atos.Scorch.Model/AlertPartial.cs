﻿namespace Atos.Scorch.Model
{
    using Atos.Scorch.Domain;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

   
    public partial class Alert
    {
        public static Alert CopyAlert(SCOMAlert alert)
        {
            Alert a = new Alert();

            a.Description = alert.AlertDescription;
            a.AlertId = alert.AlertId;
            a.Domain = alert.Domain;
            a.Name = alert.Name;
            a.NetbiosComputerName = alert.NetbiosComputerName;
            a.NetbiosDomainName = alert.NetbiosDomainName;
            a.Owner = alert.Owner;
            a.PrincipalName = alert.PrincipalName;
            a.Priority = alert.Priority;
            a.ResolvedBy = alert.ResolvedBy;
            a.Severity = alert.Severity;
            a.SiteName = alert.SiteName;
            a.TimeAlertAdded = alert.TimeAdded;
            a.TimeAlertRaised = alert.TimeRaised;
            a.TimeAlertResolved = alert.TimeResolved;


            a.Date_Added = DateTime.Now;
            a.Added_By = "SCOM Alert Web Api";
            a.Date_Last_Modified = null;
            a.Last_Modified_By = string.Empty;

            // added all published data
            a.AlertCount = alert.AlertCount;

            a.Category = alert.Category;

            a.ClassId = alert.ClassId;
        
            a.Connection = alert.Connection;

            a.CompanyKnowledge = alert.CompanyKnowledge;

            a.ProductKnowledge = alert.ProductKnowledge;

            a.ConnectorId = alert.ConnectorId;

            a.ConnectorStatus = alert.ConnectorStatus;

            a.Context = alert.Context;

            a.CustomField1 = alert.CustomField1;

            a.CustomField2 = alert.CustomField2;

            a.CustomField3 = alert.CustomField3;

            a.CustomField4 = alert.CustomField4;

            a.CustomField5 = alert.CustomField5;

            a.CustomField6 = alert.CustomField6;

            a.CustomField7 = alert.CustomField7;

            a.CustomField8 = alert.CustomField8;

            a.CustomField9 = alert.CustomField9;

            a.CustomField10 = alert.CustomField10;

            a.IsMonitorAlert = alert.IsMonitorAlert;

            a.LastModifiedByNonConnector = alert.LastModifiedByNonConnector;

            a.MaintenanceModeLastModified = alert.MaintenanceModeLastModified;

            a.ManagementGroup = alert.ManagementGroup;

            a.ManagementGroupId = alert.ManagementGroupId;

            a.ManagementPackDisplayName = alert.ManagementPackDisplayName;

            a.ManagementPackFriendlyName = alert.ManagementPackFriendlyName;

            a.ManagementPackId = alert.ManagementPackId;

            a.ManagementPackIsSealed = alert.ManagementPackIsSealed;

            a.ManagementPackMonitorName = alert.ManagementPackMonitorName;

            a.ManagementPackName = alert.ManagementPackName;

            a.ManagementPackVersion = alert.ManagementPackVersion;

            a.MonitoringClassId = alert.MonitoringClassId;

            a.MonitoringObjectDisplayName = alert.MonitoringObjectDisplayName;

            a. MonitoringObjectFullName = alert.MonitoringObjectFullName;

            a.MonitoringObjectHealthState = alert.MonitoringObjectHealthState;

            a.MonitoringObjectId = alert.MonitoringObjectId;

            a.MonitoringObjectInMaintenanceMode = alert.MonitoringObjectInMaintenanceMode;

            a.MonitoringObjectName = alert.MonitoringObjectName;

            a.MonitoringObjectPath = alert.MonitoringObjectPath;

            a.MonitoringRuleId = alert.MonitoringRuleId;

            a.MonitoringRuleName = alert.MonitoringRuleName;

            a.Parameters = alert.Parameters;

            a.ProblemId = alert.ProblemId;

            a.RepeatCount = alert.RepeatCount;

            a.ResolutionState = alert.ResolutionState;

            a.RuleId = alert.RuleId;

            a.Server = alert.Server;

            a.StateLastModified = alert.StateLastModified;

            a.TimeResolutionStateLastModified = alert.TimeResolutionStateLastModified;

            a.Username = alert.Username;

            a.MessageId = alert.MessageId;

            a.Status = alert.Status;

            return a;


        }
    }
}
