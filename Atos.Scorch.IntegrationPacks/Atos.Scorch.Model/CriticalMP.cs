﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atos.Scorch.Model
{
    public partial class CriticalMP
    {
        public int Id { get; set; }

        public string MPName { get; set; }

        public string BusinessRule { get; set; }

        public string CriticalMPName { get; set; }
    }
}
