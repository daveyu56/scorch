﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atos.Scorch.Model
{
    public class AlertModelInitializer : DropCreateDatabaseIfModelChanges <AlertModel>
    {

        public AlertModelInitializer()
        {
           // AppDomain.CurrentDomain.SetData("DataDirectory", @"D:\Project\Atos.Scorch.IntegrationPacks\Atos.Scorch.Model\AppData");
        }

        protected override void Seed (AlertModel context)
        {
          

            var mappings = new List<ITSMTemplateMapping>
            {
                new ITSMTemplateMapping { ManagementPack = "Windows", Template="SCOM Wintel", AssignToGroup = "Assigned to Engineering" },
                new ITSMTemplateMapping { ManagementPack = "RHEL", Template="SCOM RHEL", AssignToGroup = "Assigned to Engineering" },
                new ITSMTemplateMapping { ManagementPack = "EMC Storage", Template="SCOM EMC Storage", AssignToGroup = "Assigned to Engineering" },
                new ITSMTemplateMapping { ManagementPack = "Cisco UCS", Template="SCOM Cisco UCS", AssignToGroup = "Assigned to Engineering"},
                new ITSMTemplateMapping { ManagementPack = "vMware vSphere", Template="SCOM vMware vSphere", AssignToGroup = "Assigned to Engineering" },
                new ITSMTemplateMapping { ManagementPack = "SCOM", Template="SCOM SCOM", AssignToGroup = "Assigned to Engineering" },
                new ITSMTemplateMapping { ManagementPack = "SCCM", Template="SCOM SCCM", AssignToGroup = "Assigned to Engineering" },
                new ITSMTemplateMapping { ManagementPack = "SCORCH", Template="SCOM SCORCH", AssignToGroup = "Assigned to Engineering" },
                new ITSMTemplateMapping { ManagementPack = "SQL", Template="SCOM SQL", AssignToGroup = "Assigned to Engineering" }
            };

            context.ITSMTemplateMappings.AddRange(mappings);

            context.SaveChanges();
        }
            
      
    }
}
