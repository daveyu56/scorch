﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Atos.Scorch.Model
{
    public class ITSMResponseMessageConfiguration : EntityTypeConfiguration<ITSMResponseMessage>
    {
        public ITSMResponseMessageConfiguration()
        {
            this.ToTable("ITSMResponseMessage");
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(p => p.ResultCode).HasMaxLength(100);
            this.Property(p => p.Added_By).HasMaxLength(50);

           
           
        }
    }
}
