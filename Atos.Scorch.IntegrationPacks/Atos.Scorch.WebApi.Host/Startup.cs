﻿using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Microsoft.Owin.Hosting;
using System;
using System.Linq;
using Microsoft.Practices.Unity;
using System.Web.Http.Filters;
using Unity.SelfHostWebApiOwin;
using Atos.Scorch.WebApi.Host.Infrastructure;
using Atos.Scorch.WebApi.Host.Directors;
using Atos.Scorch.Model.Repositorys;

namespace Atos.Scorch.WebApi.Host
{
    public class Startup
    {

        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host. 
            var config = new HttpConfiguration();

            config.MapHttpAttributeRoutes();
            
            Register(config);

            appBuilder.UseWebApi(config);

        }

        public static void Register(HttpConfiguration config)
        {
            var container = new UnityContainer();
            container.RegisterType<IITSMDirector, ITSMDirector>(new HierarchicalLifetimeManager());
            container.RegisterType<IITSMRepository, ITSMRepository>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);

          
        }
    }

   }