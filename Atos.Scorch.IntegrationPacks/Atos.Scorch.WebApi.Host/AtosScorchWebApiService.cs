﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;
using System.Net.Http;
using System.Net;
using Microsoft.Practices.Unity;
using Unity.SelfHostWebApiOwin;
using Atos.Scorch.WebApi.Host.Directors;


namespace Atos.Scorch.WebApi.Host
{
    public partial class AtosScorchWebApiService : ServiceBase
    {
        IDisposable server;
     
        public string _serviceAddress = string.Empty;

        private const string BASE_ADDRESS = "SCOMBaseAddress";

        public AtosScorchWebApiService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var _settings = new SettingsDirector();
            
            string baseAddress = _settings.GetSettingValue(BASE_ADDRESS, string.Empty);

            // Start OWIN host 
            server = WebApp.Start<Startup>(url: baseAddress);                  
        }

        protected override void OnStop()
        {
            server.Dispose();
            server = null;
        }

        #if (DEBUG)
        public void debug()
        {
            OnStart(null);
        }
        #endif
    }
}
