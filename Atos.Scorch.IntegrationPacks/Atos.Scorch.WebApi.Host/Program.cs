﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Atos.Scorch.WebApi.Host
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static void Main()
        {



            #if (DEBUG)

                AtosScorchWebApiService service = new AtosScorchWebApiService();


                service.debug();

            #else
                   ServiceBase[] ServicesToRun;
                   ServicesToRun = new ServiceBase[] 
			       { 
				       new AtosScorchWebApiService()
			       };
                   ServiceBase.Run(ServicesToRun);
            #endif
        }
    }
}
