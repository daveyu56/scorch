﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Atos.Scorch.Domain;
using Atos.Scorch.Logging;
using Newtonsoft.Json;
using System.Web.Http.Description;
using System.Web.Http.Cors;
using Newtonsoft.Json.Linq;
using Atos.Scorch.WebApi.Host.Directors;


namespace Atos.Scorch.WebApi.Host
{
    public class SCOMController : ApiController
    {
        IITSMDirector _director;

        public SCOMController(IITSMDirector director)
        {
            _director = director;
        }

        [Route("api/scom")]
        public IHttpActionResult Get()
        {
            return Ok("SCOM Web Api Running...");
        }      

        [Route("api/scom")]
        // TODO [RequireHttps]
        public IHttpActionResult Post(JToken scomAlert)
        {
            try
            {
               

                var alert = JsonConvert.DeserializeObject<SCOMAlert>(scomAlert.ToString());

                Log.Info(string.Format("Received SCOM Alert from client : {0}", alert.ToString()));

                var itsmResponse = _director.PostAlertToITSM(alert);

                Log.Info(string.Format("Response received from ITSM : {0}", itsmResponse.ToString()));
                                   
                return Ok(itsmResponse);
                
            }
            catch (Exception e)
            {
                Log.Exception("Scorch Wep Api Error", e);
                return InternalServerError(e);
            }

        }
    }
}
