﻿using Atos.Scorch.Model.Repositorys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Atos.Scorch.WebApi.Host.Directors
{
    public class SettingsDirector : ISettingsDirector, IDisposable
    {
        private ISettingRepository _repo;

        public SettingsDirector()
        {
            _repo = new SettingRepository();
        }
        public string GetSettingValue(string name, string type)
        {
            return _repo.GetSettingValue(name, type);
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);

        }

        protected virtual void Dispose (bool disposing)
        {

            if (disposing)
            {
                if (_repo != null)
                {
                    var disposable = _repo as IDisposable;
                    disposable.Dispose();
                    disposable = null;
                }
            }
                
                
        }
    }
}
