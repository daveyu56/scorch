﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atos.Scorch.Domain;

namespace Atos.Scorch.WebApi.Host.Directors
{
    public interface IITSMDirector
    {
       ITSMResponse PostAlertToITSM(SCOMAlert alert);

    }
}
