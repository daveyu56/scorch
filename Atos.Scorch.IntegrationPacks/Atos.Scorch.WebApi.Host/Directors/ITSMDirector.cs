﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atos.Scorch.Domain;
using Atos.Scorch.Model.Repositorys;
using Atos.Scorch.Model;


namespace Atos.Scorch.WebApi.Host.Directors
{
    public class ITSMDirector : IITSMDirector
    {

        IITSMRepository _repo;

        private const string ALERT_ALREADY_POSTED = "ALERT_ALREADY_POSTED";
        private const string INVALID_MANAGEMENT_PACK = "INVALID_MANAGEMENT_PACK";
        private const string IGNORING_ALERT = "IGNORING_ALERT";
        private const string UNIX_MP_NAME = "RHEL";

        public ITSMDirector(IITSMRepository repo)
        {
            _repo = repo;
        }

        public ITSMResponse PostAlertToITSM(SCOMAlert alert)
        {

            bool OOH = false;

            Alert existingAlert = AlertAlreadyPosted(alert.AlertId);


            if (existingAlert != null && existingAlert.Status == 1)
            {
                return new ITSMResponse { ResultCode = "1", ResultMessage = ALERT_ALREADY_POSTED };
            }

            if (existingAlert != null && existingAlert.Status ==  0 && ! ProcessingManagementPack(alert.ManagementPackName))
            {
                return new ITSMResponse { ResultCode = "1", ResultMessage = INVALID_MANAGEMENT_PACK };
            }


            if (existingAlert != null)
            {
                alert.Status = 1;
            }
            else
            {
                alert.Status = 0;
            }

            OOH = IsOOH(DateTime.Now);

                   
                     
            // determine SCOM Template from alert
            var template = GetITSMTemplateFromAlert(alert.ManagementPackName, alert.MonitoringObjectPath, OOH, alert.Severity, alert.MonitoringObjectName, alert.Name, alert.MonitoringObjectDisplayName);

            if (template.ManagementPack == "IGNORE")
            {
                return new ITSMResponse { ResultCode = "1", ResultMessage = IGNORING_ALERT };
            }
            
            
            var opStatus =_repo.PostAlertToITSM(alert, template.Template);

            if (opStatus.Status)
            {
                return new ITSMResponse { ResultCode = "0", ResultMessage = opStatus.Message, AssignToGroup = template != null ? template.AssignToGroup : string.Empty};
            }
            else
            {
                return new ITSMResponse { ResultCode = "1", ResultMessage = opStatus.Message, AssignToGroup = template != null ? template.AssignToGroup : string.Empty };
            }
            
                 
           
        }

        private bool IsOOH(DateTime? alertAdded)
        {
            if (!alertAdded.HasValue)
                return false;

            if (alertAdded.Value.DayOfWeek == DayOfWeek.Saturday || alertAdded.Value.DayOfWeek == DayOfWeek.Sunday)
                return true;


            return _repo.IsOOH(alertAdded);
               

        }

        private bool ProcessingManagementPack(string packName)
        {
            return _repo.ProcessingManagementPack(packName);
        }

        private Atos.Scorch.Domain.ITSMTemplateMapping GetITSMTemplateFromAlert(string packName, string source, bool OOH, string severity, string objectName, string alertName, string displayName)
        {
            return _repo.GetITSMTemplateFromAlert(packName, source, OOH, severity, objectName, alertName, displayName);
        }

        private Alert AlertAlreadyPosted(Guid alertId)
        {
            return _repo.AlertAlreadyPosted(alertId);
        }
    }
}
