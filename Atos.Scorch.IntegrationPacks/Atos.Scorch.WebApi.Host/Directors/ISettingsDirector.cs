﻿using System;
namespace Atos.Scorch.WebApi.Host.Directors
{
    interface ISettingsDirector
    {
        string GetSettingValue(string name, string type);
    }
}
