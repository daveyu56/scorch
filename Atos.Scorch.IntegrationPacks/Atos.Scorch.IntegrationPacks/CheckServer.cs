﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SystemCenter.Orchestrator.Integration;
using System.Net;
using Atos.Scorch.IntegrationPacks.WebApiClient;
using Atos.Scorch.Domain;
using System.Collections.ObjectModel;
using Microsoft.EnterpriseManagement.Configuration;
using Microsoft.EnterpriseManagement;
using Microsoft.EnterpriseManagement.Common;
using Microsoft.EnterpriseManagement.Monitoring;

namespace Atos.Scorch.IntegrationPacks
{
    [Activity("SCOM Check Server")]
    public class CheckServer
    {
        // Inputs
        private string _netbioscomputername;
        private string _monitoringobjectpath;

        // Outputs
        private string _processServer;

        private readonly string SHOULD_PROCESS = "PROCESS";
        private readonly string SHOULD_NOT_PROCESS = "DONTPROCESS";

        private List<string> _validServers = new List<string>() { "UGBGWDB1030", "SDCBBC-WOTXD001", "UGBGWSC1036", "UGBGWSC1034", "UGBGWSC1032", "wc2-npb-cs01", "SDCBBCECCD001", "wc2-npb-fi01" };


        [ActivityInput("Netbios Computer Name")]
        public string NetbiosComputerName
        {
            get { return _netbioscomputername; }
            set { _netbioscomputername = value; }
        }

        [ActivityInput("Monitoring Object Path")]
        public string MonitoringObjectPath
        {
            get { return _monitoringobjectpath; }
            set { _monitoringobjectpath = value; }
        }

        [ActivityOutput("Process Server")]
        public string ProcessServer
        {
            get
            {
                return _processServer;
            }
        }

        [ActivityMethod()]
        public void ShouldProcessServer()
        {

            _processServer = string.Empty;

            if (string.IsNullOrEmpty(_netbioscomputername))
            {
                if (_validServers.Contains(_monitoringobjectpath.Trim()))
                {
                    _processServer = SHOULD_PROCESS;
                }
                else
                {
                    _processServer = SHOULD_NOT_PROCESS;
                }
            }
            else
            {
                if (_validServers.Contains(_netbioscomputername.Trim()))
                {
                    _processServer = SHOULD_PROCESS;
                }
                else
                {
                    _processServer = SHOULD_NOT_PROCESS;
                }
            }

            if (string.IsNullOrEmpty(_processServer))
                _processServer = SHOULD_NOT_PROCESS;


        }
       



    }
}
