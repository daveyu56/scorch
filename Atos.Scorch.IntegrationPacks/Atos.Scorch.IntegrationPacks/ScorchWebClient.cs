﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace Atos.Scorch.IntegrationPacks
{
    public class ScorchWebClient : WebClient
    {
        protected override WebRequest GetWebRequest(Uri uri)
        {
            WebRequest w = base.GetWebRequest(uri);
            w.Timeout = System.Threading.Timeout.Infinite;
            return w;
        }
    }
}
