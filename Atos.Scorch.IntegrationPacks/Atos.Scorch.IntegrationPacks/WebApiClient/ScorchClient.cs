﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using Atos.Scorch.Domain;
using Newtonsoft.Json;
using System.Configuration;
using System.Diagnostics;

namespace Atos.Scorch.IntegrationPacks.WebApiClient
{
    public class ScorchClient : IScorchClient
    {
            
        public ScorchClient() {}
      
        public OperationStatus PostAlert(SCOMAlert alert, string sURL, string proxyURL)
        {
            string assignToGroup = string.Empty;
            string json = string.Empty;

            try
            {
                json = JsonConvert.SerializeObject(alert);

                ScorchWebClient client = new ScorchWebClient();

                client.Headers.Add("content-type", @"application/json; charset=utf-8");
                client.Headers.Add("Accept", @"application/json; charset=utf-8");

                //string proxy = @"http://www-cache.reith.bbc.co.uk:80/"; // ConfigurationManager.AppSettings["Proxy"].ToString();
                ////string serviceURL = @"http://scom-connector.gateway.bbc.co.uk:85/api/scom"; // ConfigurationManager.AppSettings["ServiceURL"].ToString(); 
                //string serviceURL = @"http://bgb01ws1011.national.core.bbc.co.uk:8090/api/scom"; // ConfigurationManager.AppSettings["ServiceURL"].ToString(); 


                client.Proxy = new WebProxy(proxyURL, true);
                                               
                string jsonResponse = Encoding.ASCII.GetString(client.UploadData(sURL, "POST", Encoding.Default.GetBytes(json)));

                var response = JsonConvert.DeserializeObject<ITSMResponse>(jsonResponse);

                var requestCode = response.ResultCode;

                var requestId = response.ResultMessage;

                assignToGroup = response.AssignToGroup;
                                          
                return new OperationStatus() { OperationID = requestId, Status = (requestCode == "0") ? true : false, Message = assignToGroup};

            }
            catch (Exception e)
            {
                var opStatus = OperationStatus.CreateFromException("Scorch Client Exception - " + assignToGroup + e.Message, e);

                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "Application";
                    eventLog.WriteEntry(e.Message, EventLogEntryType.Error, 101, 1);
                    eventLog.WriteEntry(e.StackTrace, EventLogEntryType.Error, 101, 1);
                    eventLog.WriteEntry(json, EventLogEntryType.Error, 101, 1);

                    if (e.InnerException != null)
                        eventLog.WriteEntry(e.InnerException.Message, EventLogEntryType.Error, 101, 1);
                } 

                return opStatus;
            }
        }
    }
}
