﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atos.Scorch.Domain;


namespace Atos.Scorch.IntegrationPacks.WebApiClient
{
    public interface IScorchClient
    {
        OperationStatus PostAlert(SCOMAlert alert, string serviceURL, string proxyURL);
    }
}
