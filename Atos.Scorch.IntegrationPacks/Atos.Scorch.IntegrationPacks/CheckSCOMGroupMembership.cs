﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SystemCenter.Orchestrator.Integration;
using Microsoft.EnterpriseManagement;
using Microsoft.EnterpriseManagement.Common;
using Microsoft.EnterpriseManagement.Configuration;
using Microsoft.EnterpriseManagement.Monitoring;

namespace Atos.Scorch.IntegrationPacks
{
    [Activity("Atos Check SCOM Group Membership")]
    public class CheckSCOMGroupMembership
    {
        private string _monitoringObjectId = String.Empty;
        private string _groupName = String.Empty;
        private string _rootServerName = String.Empty;
        private string _message = String.Empty;
        private bool _result = false;

        [ActivityInput("MonitoringObjectId")]
        public string MonitoringObjectId
        {
            set { _monitoringObjectId = value; }
        }

        [ActivityInput("SCOM Root Server FQDN")]
        public string RootServerName
        {
            set { _rootServerName = value; }
        }

        [ActivityInput("SCOM Group Name")]
        public string SCOMGroupName
        {
            set { _groupName = value; }
        }

        [ActivityOutput("Is Member of SCOM Group")]
        public bool IsMemberOfSCOMGroup
        {
            get
            {
                return _result;
            }
        }

        [ActivityOutput("Exception Message")]
        public string ExceptionMessage
        {
            get
            {
                return _message;
            }
        }

        [ActivityMethod()]
        public void Validate()
        {
            try
            {
                using (ManagementGroup manGroup = new ManagementGroup(_rootServerName))
                {
                    MonitoringObject monObject =  manGroup.EntityObjects.GetObject<MonitoringObject>(new Guid(_monitoringObjectId), ObjectQueryOptions.Default);

                    if (monObject == null)
                    {
                        _message = "MonitoringObject not found for ID: " + _monitoringObjectId;
                        _result = false;

                        return;
                    }


                    IList<MonitoringObjectGroup>  groups = manGroup.EntityObjects.GetRootObjectGroups<MonitoringObjectGroup>(ObjectQueryOptions.Default);
                    MonitoringObjectGroup targetGroup = null;

                    foreach (MonitoringObjectGroup group in groups)
                    {
                        if (group.DisplayName == _groupName)
                        {
                            targetGroup = group;
                            break;
                        }
                    }

                    if (targetGroup == null)
                    {
                        _message = "SCOM group does not exist";
                        _result = false;
                    }
                    else
                    {
                        IList<ManagementPackClass> monitoringClasses = targetGroup.GetClasses();
                        ManagementPackRelationship manPackRelationship = manGroup.EntityTypes.GetRelationshipClass(SystemRelationship.Containment);
                        EnterpriseManagementRelationshipObject<EnterpriseManagementObject> result = manGroup.EntityObjects.GetRelationshipObjectsWhereTarget<EnterpriseManagementObject>(new Guid(_monitoringObjectId), manPackRelationship, DerivedClassTraversalDepth.Recursive, TraversalDepth.Recursive, ObjectQueryOptions.Default).FirstOrDefault(d => !d.IsDeleted && d.SourceObject.GetLeastDerivedNonAbstractClass().GetBaseType().IsSubtypeOf(monitoringClasses[0]));

                        if (result != null)
                        {
                            // Return success to calling runbook
                            _result = true;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                _message = ex.Message;
                _result = false;
            }
        }
    }
}
