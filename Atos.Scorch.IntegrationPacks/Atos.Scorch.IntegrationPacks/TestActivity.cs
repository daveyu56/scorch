﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SystemCenter.Orchestrator.Integration;
using System.Net;

namespace Atos.Scorch.IntegrationPacks
{
    [Activity("Atos Test")]
    public class TestActivity
    {
        private string _alertID;

        [ActivityInput("Alert ID")]
        public string AlertID
        {
            set { _alertID = value; }
        }

        [ActivityOutput("Incident ID")]
        public string IncidentID
        {
            get
            {
                var r = new Random();
                return "INC" + r.Next(1500, 50000).ToString("0000000000");
            }
        }

        [ActivityMethod()]
        public void CreateITSMTicket()
        {
            using (WebClient client = new WebClient())
            {
                client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                string response = client.UploadString("http://localhost/api/scom", "POST", "data");

                if (response != null)
                {

                }
 
            }
        }
    }
}
