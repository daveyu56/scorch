﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SystemCenter.Orchestrator.Integration;
using System.Net;
using Atos.Scorch.IntegrationPacks.WebApiClient;
using Atos.Scorch.Domain;
using System.Collections.ObjectModel;
using Microsoft.EnterpriseManagement.Configuration;
using Microsoft.EnterpriseManagement;
using Microsoft.EnterpriseManagement.Common;
using Microsoft.EnterpriseManagement.Monitoring;

namespace Atos.Scorch.IntegrationPacks
{
    [Activity("SCOM ITSM Alert")]
    public class ScorchActivity
    {

        // Inputs
        //private string _rootServerName = String.Empty;

        private Guid _alertId;
        private string _description;
    //    private string _domain;

        private string _name;

    //    private string _netbiosComputerName;

  //      private string _netbiosDomainName;

   //     private string _owner;

   //     private string _principalName;

          private string _priority;

   //     private string _resolvedBy;

          private string _severity;

   //     private string _siteName;

          private string _timeAdded;

   //     private DateTime? _timeRaised;

    //    private DateTime? _timeResolved;

        // added all published columns
  //      private int _alertCount;

    //    private string _category;

   //     private Guid _classId;

   //     private string _connection;

   //     private string _companyKnowledge { get; set; }

  //      private Guid _connectorId;

  //      private string _connectorStatus;

  //      private string _context;

  //      private string _customField1;

   //     private string _customField2;

  //      private string _customField3;

  //      private string _customField4;

  //      private string _customField5;

 //       private string _customField6;

  //      private string _customField7;

  //      private string _customField8;

  //      private string _customField9;

 //       private string _customField10;

  //      private bool _isMonitorAlert;

   //     private DateTime? _lastModifiedByNonConnector;

 //       private DateTime? _maintenanceModeLastModified;

 //       private string _managementGroup;

 //       private Guid _managementGroupId;

  //      private string _managementPackDisplayName;

  //      private string _managementPackFriendlyName;

  //      private Guid _managementPackId;

  //      private bool _managementPackIsSealed;

  //      private string _managementPackMonitorName;

        private string _managementPackName;

 //       private string _managementPackVersion;

  //      private Guid _monitoringClassId;

        private string _monitoringObjectDisplayName;

        private string _monitoringObjectFullName;

  //      private string _monitoringObjectHealthState;

        private Guid _monitoringObjectId;

    //    private bool _monitoringObjectInMaintenanceMode;

        private string _monitoringObjectName;

        private string _monitoringObjectPath;

    //    private Guid _monitoringRuleId;

    //    private string _monitoringRuleName;

   //     private string _parameters;

   //     private Guid _problemId;

     //   private int _repeatCount;

    //    private string _resolutionState;

        private string _outputResolutionState;

    //    private Guid _ruleId;

        private string _server;

    //    private DateTime? _stateLastModified;

//        private DateTime? _timeResolutionStateLastModified;

     //   private string _username;


        // Outputs
        private string _incidentId;
        private const string FAILED_TO_CREATE_TICKET = "FAILED";
        private string EXCEPTION_CAUGHT = "EXCEPTION";
        private string _serviceURL;
        private string _proxyURL;

        //[ActivityInput("SCOM Root Server FQDN")]
        //public string RootServerName
        //{
        //    set { _rootServerName = value; }
        //}

      
        [ActivityInput("Alert ID")]
        public Guid AlertID
        {
            get { return _alertId; }
            set { _alertId = value; }

        }

        [ActivityInput("Description")]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        //[ActivityInput("Domain")]
        //public string Domain
        //{
        //    get { return _domain; }
        //    set { _domain = value; }
        //}

        [ActivityInput("Name")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        //[ActivityInput("NetbiosComputerName")]
        //public string NetbiosComputerName
        //{
        //    get { return _netbiosComputerName; }
        //    set { _netbiosComputerName = value; }
        //}

        //[ActivityInput("NetbiosDomainName")]
        //public string NetbiosDomainName
        //{
        //    get { return _netbiosDomainName; }
        //    set { _netbiosDomainName = value; }
        //}

        //[ActivityInput("Owner")]
        //public string Owner
        //{
        //    get { return _owner; }
        //    set { _owner = value; }
        //}

        //[ActivityInput("PrincipalName")]
        //public string PrincipalName
        //{
        //    get { return _principalName; }
        //    set { _principalName = value; }
        //}

        [ActivityInput("Priority")]
        public string Priority
        {
            get { return _priority; }
            set { _priority = value; }
        }

        //[ActivityInput("ResolvedBy")]
        //public string ResolvedBy
        //{
        //    get { return _resolvedBy; }
        //    set { _resolvedBy = value; }
        //}

        [ActivityInput("Severity")]
        public string Severity
        {
            get { return _severity; }
            set { _severity = value; }
        }

        //[ActivityInput("SiteName")]
        //public string SiteName
        //{
        //    get { return _siteName; }
        //    set { _siteName = value; }
        //}

        [ActivityInput("TimeAdded")]
        public string TimeAdded
        {
            get { return _timeAdded; }
            set { _timeAdded = value; }
        }

        //[ActivityInput("TimeRaised")]
        //public DateTime? TimeRaised
        //{
        //    get { return _timeRaised; }
        //    set { _timeRaised = value; }
        //}

        //[ActivityInput("TimeResolved")]
        //public DateTime? TimeResolved
        //{
        //    get { return _timeResolved; }
        //    set { _timeResolved = value; }
        //}

        //[ActivityInput("AlertCount")]
        //public int AlertCount
        //{
        //    get { return _alertCount; }
        //    set { _alertCount = value; }
        //}

        //[ActivityInput("Category")]
        //public string Category
        //{
        //    get { return _category; }
        //    set { _category = value; }
        //}

        //[ActivityInput("Class ID")]
        //public Guid ClassId
        //{
        //    get { return _classId; }
        //    set { _classId = value; }
        //}

        //[ActivityInput("Connection")]
        //public string Connection
        //{
        //    get { return _connection; }
        //    set { _connection = value; }
        //}

        //[ActivityInput("Company Knowledge")]
        //public string CompanyKnowledge
        //{
        //    get { return _companyKnowledge; }
        //    set { _companyKnowledge = value; }
        //}

        //[ActivityInput("ConnectorId")]
        //public Guid ConnectorId
        //{
        //    get { return _connectorId; }
        //    set { _connectorId = value; }
        //}

        //[ActivityInput("ConnectorStatus")]
        //public string ConnectorStatus
        //{
        //    get { return _connectorStatus; }
        //    set { _connectorStatus = value; }
        //}

        //[ActivityInput("Context")]
        //public string Context
        //{
        //    get { return _context; }
        //    set { _context = value; }
        //}

        //[ActivityInput("CustomField1")]
        //public string CustomField1
        //{
        //    get { return _customField1; }
        //    set { _customField1 = value; }
        //}


        //[ActivityInput("CustomField2")]
        //public string CustomField2
        //{
        //    get { return _customField2; }
        //    set { _customField2 = value; }
        //}

        //[ActivityInput("CustomField3")]
        //public string CustomField3
        //{
        //    get { return _customField3; }
        //    set { _customField3 = value; }
        //}

        //[ActivityInput("CustomField4")]
        //public string CustomField4
        //{
        //    get { return _customField4; }
        //    set { _customField4 = value; }
        //}

        //[ActivityInput("CustomField5")]
        //public string CustomField5
        //{
        //    get { return _customField5; }
        //    set { _customField5 = value; }
        //}

        //[ActivityInput("CustomField6")]
        //public string CustomField6
        //{
        //    get { return _customField6; }
        //    set { _customField6 = value; }
        //}

        //[ActivityInput("CustomField7")]
        //public string CustomField7
        //{
        //    get { return _customField7; }
        //    set { _customField7 = value; }
        //}

        //[ActivityInput("CustomField8")]
        //public string CustomField8
        //{
        //    get { return _customField8; }
        //    set { _customField8 = value; }
        //}

        //[ActivityInput("CustomField9")]
        //public string CustomField9
        //{
        //    get { return _customField9; }
        //    set { _customField9 = value; }
        //}

        //[ActivityInput("CustomField10")]
        //public string CustomField10
        //{
        //    get { return _customField10; }
        //    set { _customField10 = value; }
        //}


        //[ActivityInput("IsMonitorAlert")]
        //public bool IsMonitorAlert
        //{
        //    get { return _isMonitorAlert; }
        //    set { _isMonitorAlert = value; }
        //}

        //[ActivityInput("LastModifiedByNonConnector")]
        //public DateTime? LastModifiedByNonConnector
        //{
        //    get { return _lastModifiedByNonConnector; }
        //    set { _lastModifiedByNonConnector = value; }
        //}

        //[ActivityInput("MaintenanceModeLastModified")]
        //public DateTime? MaintenanceModeLastModified
        //{
        //    get { return _maintenanceModeLastModified; }
        //    set { _maintenanceModeLastModified = value; }
        //}

        //[ActivityInput("ManagementGroup")]
        //public string ManagementGroup
        //{
        //    get { return _managementGroup; }
        //    set { _managementGroup = value; }
        //}

        //[ActivityInput("ManagementGroupId")]
        //public Guid ManagementGroupId
        //{
        //    get { return _managementGroupId; }
        //    set { _managementGroupId = value; }
        //}

        //[ActivityInput("ManagementPackDisplayName")]
        //public string ManagementPackDisplayName
        //{
        //    get { return _managementPackDisplayName; }
        //    set { _managementPackDisplayName = value; }
        //}

        //[ActivityInput("ManagementPackFriendlyName")]
        //public string ManagementPackFriendlyName
        //{
        //    get { return _managementPackFriendlyName; }
        //    set { _managementPackFriendlyName = value; }
        //}

        //[ActivityInput("ManagementPackId")]
        //public Guid ManagementPackId
        //{
        //    get { return _managementPackId; }
        //    set { _managementPackId = value; }
        //}

        //[ActivityInput("ManagementPackIsSealed")]
        //public bool ManagementPackIsSealed
        //{
        //    get { return _managementPackIsSealed; }
        //    set { _managementPackIsSealed = value; }
        //}

        //[ActivityInput("ManagementPackMonitorName")]
        //public string ManagementPackMonitorName
        //{
        //    get { return _managementPackMonitorName; }
        //    set { _managementPackMonitorName = value; }
        //}

        [ActivityInput("ManagementPackName")]
        public string ManagementPackName
        {
            get { return _managementPackName; }
            set { _managementPackName = value; }
        }

        //[ActivityInput("ManagementPackVersion")]
        //public string ManagementPackVersion
        //{
        //    get { return _managementPackVersion; }
        //    set { _managementPackVersion = value; }
        //}

        //[ActivityInput("MonitoringClassId")]
        //public Guid MonitoringClassId
        //{
        //    get { return _monitoringClassId; }
        //    set { _monitoringClassId = value; }
        //}

        [ActivityInput("MonitoringObjectDisplayName")]
        public string MonitoringObjectDisplayName
        {
            get { return _monitoringObjectDisplayName; }
            set { _monitoringObjectDisplayName = value; }
        }

        [ActivityInput("MonitoringObjectFullName")]
        public string MonitoringObjectFullName
        {
            get { return _monitoringObjectFullName; }
            set { _monitoringObjectFullName = value; }
        }

        //[ActivityInput("MonitoringObjectHealthState")]
        //public string MonitoringObjectHealthState
        //{
        //    get { return _monitoringObjectHealthState; }
        //    set { _monitoringObjectHealthState = value; }
        //}

        [ActivityInput("MonitoringObjectId")]
        public Guid MonitoringObjectId
        {
            get { return _monitoringObjectId; }
            set { _monitoringObjectId = value; }
        }

        //[ActivityInput("MonitoringObjectInMaintenanceMode")]
        //public bool MonitoringObjectInMaintenanceMode
        //{
        //    get { return _monitoringObjectInMaintenanceMode; }
        //    set { _monitoringObjectInMaintenanceMode = value; }
        //}

        [ActivityInput("MonitoringObjectName")]
        public string MonitoringObjectName
        {
            get { return _monitoringObjectName; }
            set { _monitoringObjectName = value; }
        }

        [ActivityInput("MonitoringObjectPath")]
        public string MonitoringObjectPath
        {
            get { return _monitoringObjectPath; }
            set { _monitoringObjectPath = value; }
        }

        //[ActivityInput("MonitoringRuleId")]
        //public Guid MonitoringRuleId
        //{
        //    get { return _monitoringRuleId; }
        //    set { _monitoringRuleId = value; }
        //}

        //[ActivityInput("MonitoringRuleName")]
        //public string MonitoringRuleName
        //{
        //    get { return _monitoringRuleName; }
        //    set { _monitoringRuleName = value; }
        //}

        //[ActivityInput("Parameters")]
        //public string Parameters
        //{
        //    get { return _parameters; }
        //    set { _parameters = value; }
        //}

        //[ActivityInput("ProblemId")]
        //public Guid ProblemId
        //{
        //    get { return _problemId; }
        //    set { _problemId = value; }
        //}

        //[ActivityInput("RepeatCount")]
        //public int RepeatCount
        //{
        //    get { return _repeatCount; }
        //    set { _repeatCount = value; }
        //}

        //[ActivityInput("ResolutionState")]
        //public string ResolutionState
        //{
        //    get { return _resolutionState; }
        //    set { _resolutionState = value; }
        //}

        //[ActivityInput("RuleId")]
        //public Guid RuleId
        //{
        //    get { return _ruleId; }
        //    set { _ruleId = value; }
        //}

        [ActivityInput("Server")]
        public string Server
        {
            get { return _server; }
            set { _server = value; }
        }

        //[ActivityInput("StateLastModified")]
        //public DateTime? StateLastModified
        //{
        //    get { return _stateLastModified; }
        //    set { _stateLastModified = value; }
        //}

        //[ActivityInput("TimeResolutionStateLastModified")]
        //public DateTime? TimeResolutionStateLastModified
        //{
        //    get { return _timeResolutionStateLastModified; }
        //    set { _timeResolutionStateLastModified = value; }
        //}

        //[ActivityInput("Username")]
        //public string Username
        //{
        //    get { return _username; }
        //    set { _username = value; }
        //}

        [ActivityInput("ServiceURL")]
        public string ServiceURL
        {
            get { return _serviceURL; }
            set { _serviceURL = value; }

        }

        [ActivityInput("ProxyURL")]
        public string ProxyURL
        {
            get { return _proxyURL; }
            set { _proxyURL = value; }

        }

        [ActivityOutput("Incident ID")]
        public string IncidentID
        {
            get
            {
                return _incidentId;
            }
        }

        [ActivityOutput("OutputResolutionState")]
        public string OutputResolutionState
        {
            get
            {
                return _outputResolutionState;
            }
        }

      

        [ActivityMethod()]
        public void CreateITSMTicket()
        {
            _outputResolutionState = string.Empty;

            try
            {
              
                var alert = new SCOMAlert();
                
                Configure(alert);

                var scorchClient = new ScorchClient();

                var opStatus = scorchClient.PostAlert(alert, _serviceURL, _proxyURL);

                if (opStatus.Status)
                {
                    _incidentId = opStatus.OperationID.ToString();
                    _outputResolutionState = opStatus.Message;

                }
                else
                {
                    _incidentId = FAILED_TO_CREATE_TICKET;
                    _outputResolutionState = opStatus.Message;
                }
              
            }
            catch (Exception e)
            {
                _incidentId = EXCEPTION_CAUGHT;
               
            }

        }

        private void Configure(SCOMAlert alert)
        {
            alert.AlertId = _alertId;
            alert.AlertDescription = _description.Replace("\"", "");
         //   alert.Domain = _domain;
            alert.Name = _name;
           // alert.NetbiosComputerName = _netbiosComputerName;
           // alert.NetbiosDomainName = _netbiosDomainName;
           // alert.Owner = _owner;
           // alert.PrincipalName = _principalName;
              alert.Priority = _priority;
           // alert.ResolvedBy = _resolvedBy;
              alert.Severity = _severity;
           // alert.SiteName = _siteName;

            int yr;
            int mth;
            int day;
            int hr;
            int mi;
            int sec;

         
            string dateComponent = TimeAdded.Split('T')[0];
            string timeComponent =  TimeAdded.Split('T')[1];

            yr = int.Parse(dateComponent.Substring(0, 4));
            mth  = int.Parse(dateComponent.Substring(5, 2));
            day = int.Parse(dateComponent.Substring(8, 2));

            hr = int.Parse(timeComponent.Substring(0, 2));
            mi = int.Parse(timeComponent.Substring(3, 2));
            sec = int.Parse(timeComponent.Substring(6, 2));

            alert.TimeAdded = new DateTime(yr, mth, day, hr, mi, sec);

           // alert.TimeRaised = _timeRaised;
           // alert.TimeResolved = _timeResolved;

            // added all published data items
       //     alert.AlertCount = _alertCount;

        //    alert.Category = _category;

       //     alert.ClassId = _classId;

         //   alert.Connection = _connection;

       //     alert.CompanyKnowledge = _companyKnowledge;

        //    alert.ConnectorId = _connectorId;

       //     alert.ConnectorStatus = _connectorStatus;

       //     alert.Context = _context;

       //     alert.CustomField1 = _customField1;

       //     alert.CustomField2 = _customField2;

            //alert.CustomField3 = _customField3;

            //alert.CustomField4 = _customField4;

            //alert.CustomField5 = _customField5;

            //alert.CustomField6 = _customField6;

            //alert.CustomField7 = _customField7;

            //alert.CustomField8 = _customField8;

            //alert.CustomField9 = _customField9;

            //alert.CustomField10 = _customField10;

            //alert.IsMonitorAlert = _isMonitorAlert;

          //  alert.LastModifiedByNonConnector = LastModifiedByNonConnector;

          //  alert.MaintenanceModeLastModified = _maintenanceModeLastModified;

          //  alert.ManagementGroup = _managementGroup;

         //   alert.ManagementGroupId = _managementGroupId;

            //alert.ManagementPackDisplayName = _managementPackDisplayName;

            //alert.ManagementPackFriendlyName = _managementPackFriendlyName;

            //alert.ManagementPackId = _managementPackId;

            //alert.ManagementPackIsSealed = _managementPackIsSealed;

            //alert.ManagementPackMonitorName = _managementPackMonitorName;

            alert.ManagementPackName = _managementPackName;

            //alert.ManagementPackVersion = _managementPackVersion;

           // alert.MonitoringClassId = _monitoringClassId;

            alert.MonitoringObjectDisplayName = _monitoringObjectDisplayName;

            alert.MonitoringObjectFullName = _monitoringObjectFullName;

          //  alert.MonitoringObjectHealthState = _monitoringObjectHealthState;

            alert.MonitoringObjectId = _monitoringObjectId;

          //  alert.MonitoringObjectInMaintenanceMode = _monitoringObjectInMaintenanceMode;

            alert.MonitoringObjectName = _monitoringObjectName ?? string.Empty;

            alert.MonitoringObjectPath = _monitoringObjectPath;

           // alert.MonitoringRuleId = _monitoringRuleId;

          //  alert.MonitoringRuleName = _monitoringRuleName;

          //  alert.Parameters = _parameters;

         //   alert.ProblemId = _problemId;

         //   alert.RepeatCount = _repeatCount;

          //  alert.ResolutionState = _resolutionState;

          //  alert.RuleId = _ruleId;

            alert.Server = _server;

         //   alert.StateLastModified = _stateLastModified;

          //  alert.TimeResolutionStateLastModified = _timeResolutionStateLastModified;

           // alert.Username = _username;

            alert.MessageId = Guid.NewGuid();

            alert.Status = 0;

            string c = string.Empty;

            StringBuilder sb = new StringBuilder();

            GetProductKnowledge(alert, sb);

          

        }

        private void GetProductKnowledge(SCOMAlert alert, StringBuilder sb)
        {

            //using (ManagementGroup manGroup = new ManagementGroup(_rootServerName))
            //{
            //    Guid id = (alert.IsMonitorAlert) ? alert.ProblemId : alert.MonitoringRuleId;

            //    var articles = manGroup.Knowledge.GetKnowledgeArticles(id).Where(p => p.GetManagementPack().Name == alert.ManagementPackName);

            //    foreach (var article in articles)
            //    {
            //        if (article.HtmlContent != null)
            //        {
            //            Get the HTML content.
            //            sb.AppendLine(article.HtmlContent.ToString());
            //        }
            //        else if (article.MamlContent != null)
            //        {
            //            Get the MAML content.
            //            sb.AppendLine(article.MamlContent.ToString());
            //        }
            //    }
           // }

            alert.ProductKnowledge = string.Empty; // sb.ToString();
        }
    }
}
