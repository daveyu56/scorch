﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Atos.Scorch.Domain
{
    public class ITSMResponse
    {
        public string ResultCode { get; set; }

        public string ResultMessage { get; set; }

        public string AssignToGroup { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(string.Format("ResultCode: {0} ", ResultCode));
            sb.Append(string.Format("ResultMessage: {0}", ResultMessage));
            sb.Append(string.Format("AssignToGroup: {0}", AssignToGroup));

            return sb.ToString();
        }
    }
}
