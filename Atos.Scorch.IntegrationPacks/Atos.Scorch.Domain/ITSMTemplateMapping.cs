﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atos.Scorch.Domain
{
    public class ITSMTemplateMapping
    {
        public string ManagementPack { get; set; }
        public string Template { get; set; }
        public string AssignToGroup { get; set; }
    }
}
