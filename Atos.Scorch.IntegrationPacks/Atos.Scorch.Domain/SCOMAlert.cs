﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Atos.Scorch.Domain
{
    public class SCOMAlert
    {
        public Guid AlertId { get; set; }

        public string AlertDescription { get; set; }

        public string Domain { get; set; }

        public string Name { get; set; }

        public string NetbiosComputerName { get; set; }

        public string NetbiosDomainName { get; set; }

        public string Owner { get; set; }

        public string PrincipalName { get; set; }

        public string Priority { get; set; }

        public string ResolvedBy { get; set; }

        public string Severity { get; set; }

        public string SiteName { get; set; }

        public DateTime? TimeAdded { get; set; }

        public DateTime? TimeRaised { get; set; }

        public DateTime? TimeResolved { get; set; }

        // added all published data columns
        public int AlertCount { get; set; }

        public string Category { get; set; }

        public Guid ClassId { get; set; }

        public string Connection { get; set; }

        public string CompanyKnowledge { get; set; }

        public Guid ConnectorId { get; set; }

        public string ConnectorStatus { get; set; }

        public string Context { get; set; }

        public string CustomField1 { get; set; }

        public string CustomField2 { get; set; }

        public string CustomField3 { get; set; }

        public string CustomField4 { get; set; }

        public string CustomField5 { get; set; }

        public string CustomField6 { get; set; }

        public string CustomField7 { get; set; }

        public string CustomField8 { get; set; }

        public string CustomField9 { get; set; }

        public string CustomField10 { get; set; }

        public bool IsMonitorAlert { get; set; }

        public DateTime? LastModifiedByNonConnector { get; set; }

        public DateTime? MaintenanceModeLastModified { get; set; }

        public string ManagementGroup { get; set; }

        public Guid ManagementGroupId { get; set; }

        public string ManagementPackDisplayName { get; set; }

        public string ManagementPackFriendlyName { get; set; }

        public Guid ManagementPackId { get; set; }

        public bool ManagementPackIsSealed { get; set; }

        public string ManagementPackMonitorName { get; set; }

        public string ManagementPackName { get; set; }

        public string ManagementPackVersion { get; set; }

        public Guid MonitoringClassId { get; set; }

        public string MonitoringObjectDisplayName { get; set; }

        public string MonitoringObjectFullName { get; set; }

        public string MonitoringObjectHealthState { get; set; }

        public Guid MonitoringObjectId { get; set; }

        public bool MonitoringObjectInMaintenanceMode { get; set; }

        public string MonitoringObjectName { get; set; }

        public string MonitoringObjectPath { get; set; }

        public Guid MonitoringRuleId { get; set; }

        public string MonitoringRuleName { get; set; }

        public string Parameters { get; set; }

        public Guid ProblemId { get; set; }

        public int RepeatCount { get; set; }

        public string ResolutionState { get; set; }

        public Guid RuleId { get; set; }

        public string Server { get; set; }

        public DateTime? StateLastModified { get; set; }

        public DateTime? TimeResolutionStateLastModified { get; set; }

        public string Username { get; set; }

        public string ProductKnowledge { get; set; }

        public Guid MessageId { get; set; }

        public int Status { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(string.Format("AlertId: {0} ", AlertId));
            sb.Append(string.Format("Alert Description: {0} ", AlertDescription));
          //  sb.Append(string.Format("Domain: {0} ", Domain));
            sb.Append(string.Format("Name: {0} ", Name));
          //  sb.Append(string.Format("NetbiosComputerName: {0} ", NetbiosComputerName));
         //   sb.Append(string.Format("NetbiosDomainName: {0} ", NetbiosDomainName));
         //   sb.Append(string.Format("Owner: {0} ", Owner));
         //   sb.Append(string.Format("PrincipalName: {0} ", PrincipalName));
            sb.Append(string.Format("Priority: {0} ", Priority));
         //   sb.Append(string.Format("ResolvedBy: {0} ", ResolvedBy));
            sb.Append(string.Format("Severity: {0} ", Severity));
         //   sb.Append(string.Format("SiteName: {0} ", SiteName));
              sb.Append(string.Format("TimeAdded: {0} ", TimeAdded == null ? string.Empty : TimeAdded.ToString()));
          //  sb.Append(string.Format("TimeRaised: {0} ", TimeRaised == null ? string.Empty : TimeRaised.ToString()));
           // sb.Append(string.Format("TimeResolved: {0}", TimeResolved == null ? string.Empty : TimeResolved.ToString()));

            // added all published fields
            //sb.Append(string.Format("AlertCount: {0} ", AlertCount.ToString()));

            //sb.Append(string.Format("Category: {0} ", Category));

            //sb.Append(string.Format("ClassId: {0} ", ClassId));

            //sb.Append(string.Format("Connection: {0} ", Connection));

            //sb.Append(string.Format("CompanyKnowledge: {0} ", CompanyKnowledge));

            //sb.Append(string.Format("ProductKnowledge: {0} ", ProductKnowledge));

            //sb.Append(string.Format("ConnectorId: {0} ", ConnectorId));

            //sb.Append(string.Format("ConnectorStatus: {0} ", ConnectorStatus));

            //sb.Append(string.Format("Context: {0} ", Context));

            ///sb.Append(string.Format("CustomField1: {0} ", CustomField1));

            //sb.Append(string.Format("CustomField2: {0} ", CustomField2));

            //sb.Append(string.Format("CustomField3: {0} ", CustomField3));

            //sb.Append(string.Format("CustomField4: {0} ", CustomField4));

            //sb.Append(string.Format("CustomField5: {0} ", CustomField5));

            //sb.Append(string.Format("CustomField6: {0} ", CustomField6));

            //sb.Append(string.Format("CustomField7: {0} ", CustomField7));

            //sb.Append(string.Format("CustomField8: {0} ", CustomField8));

            //sb.Append(string.Format("CustomField9: {0} ", CustomField9));

            //sb.Append(string.Format("CustomField10: {0} ", CustomField10));

          //  sb.Append(string.Format("IsMonitorAlert: {0} ", (IsMonitorAlert) ? "Yes": "No"));

          //  sb.Append(string.Format("LastModifiedByNonConnector: {0} ", LastModifiedByNonConnector == null ? string.Empty : LastModifiedByNonConnector.ToString()));

           // sb.Append(string.Format("MaintenanceModeLastModified: {0} ", MaintenanceModeLastModified));

           // sb.Append(string.Format("ManagementGroup: {0} ", ManagementGroup));

            //sb.Append(string.Format("ManagementGroupId: {0} ", ManagementGroupId));

            //sb.Append(string.Format("ManagementPackDisplayName: {0} ", ManagementPackDisplayName));

           // sb.Append(string.Format("ManagementPackFriendlyName: {0} ", ManagementPackFriendlyName));

            //sb.Append(string.Format("ManagementPackId: {0} ", ManagementPackId));

           // sb.Append(string.Format("ManagementPackIsSealed: {0} ", (ManagementPackIsSealed) ? "Yes": "No"));

          //  sb.Append(string.Format("ManagementPackMonitorName: {0} ", ManagementPackMonitorName));

            sb.Append(string.Format("ManagementPackName: {0} ", ManagementPackName));

           // sb.Append(string.Format("ManagementPackVersion: {0} ", ManagementPackVersion));

           // sb.Append(string.Format("MonitoringClassId: {0} ", MonitoringClassId));

            sb.Append(string.Format("MonitoringObjectDisplayName: {0} ", MonitoringObjectDisplayName));

            sb.Append(string.Format("MonitoringObjectFullName: {0} ", MonitoringObjectFullName));

            //sb.Append(string.Format("MonitoringObjectHealthState: {0} ", MonitoringObjectHealthState));

            sb.Append(string.Format("MonitoringObjectId: {0} ", MonitoringObjectId));

           // sb.Append(string.Format("MonitoringObjectInMaintenanceMode: {0} ", (MonitoringObjectInMaintenanceMode) ? "Yes": "No"));

           // sb.Append(string.Format("MonitoringObjectName: {0} ", MonitoringObjectName));

           // sb.Append(string.Format("MonitoringObjectPath: {0} ", MonitoringObjectPath));

           // sb.Append(string.Format("MonitoringRuleId: {0} ", MonitoringRuleId));

           // sb.Append(string.Format("MonitoringRuleName: {0} ", MonitoringRuleName));

            //sb.Append(string.Format("Parameters: {0} ", Parameters));

            //sb.Append(string.Format("ProblemId: {0} ", ProblemId));

            //sb.Append(string.Format("RepeatCount: {0} ", RepeatCount.ToString()));

            //sb.Append(string.Format("ResolutionState: {0} ", ResolutionState.ToString()));

            //sb.Append(string.Format("RuleId: {0} ", RuleId));

            sb.Append(string.Format("Server: {0} ", Server));

           // sb.Append(string.Format("StateLastModified: {0} ", StateLastModified == null ? string.Empty : StateLastModified.ToString()));

           // sb.Append(string.Format("TimeResolutionStateLastModified: {0} ", TimeResolutionStateLastModified == null ? string.Empty : TimeResolutionStateLastModified.ToString()));

           // sb.Append(string.Format("Username: {0} ", Username));

            sb.Append(string.Format("MessageId: {0} ", MessageId.ToString()));

            sb.Append(string.Format("Status: {0} ", Status.ToString()));

            string s = sb.ToString();
            return s.Substring(0, s.Length > 1500 ? 1500 :  s.Length - 1  );
        }


        
    }
}