﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Atos.Scorch.IntegrationPacks.WebApiClient;
using Atos.Scorch.Domain;
using Newtonsoft.Json;

namespace Atos.Scorch.WebApi.Client.Tests
{
    [TestClass]
    public class ScorchClientTests
    {
        [TestMethod]
        public void Scorch_Client_Returns_Result()
        {
            IScorchClient scorchClient = new ScorchClient();

            var alert = new SCOMAlert();

            alert.ManagementPackName = @"Microsoft.Linux.RHEL.6";
            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"The thresholds for the Logical Disk Free Space monitor have been exceeded. The values that exceeded the threshold are: 9% and 918MB Free Space.";
            alert.TimeAdded = new DateTime(2015, 3, 23, 12, 3, 23);
            alert.MonitoringObjectDisplayName = @"/usr";
            alert.MonitoringObjectFullName = @"Microsoft.Linux.RHEL.6.LogicalDisk:SDCBBCSSMD001;/usr";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.MonitoringObjectName = @"/usr";
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Status = 0;
            alert.Priority = "High";
            alert.Severity = "Critical";
            alert.Name = "Logical disk space is too low IGNORE TEST";
             string proxyURL = @"http://www-cache.reith.bbc.co.uk:80/"; // ConfigurationManager.AppSettings["Proxy"].ToString();
                //string serviceURL = @"http://scom-connector.gateway.bbc.co.uk:85/api/scom"; // ConfigurationManager.AppSettings["ServiceURL"].ToString(); 
             string serviceURL = @"http://scom-connector.gateway.bbc.co.uk:85/api/scom"; // Co

            var opStatus = scorchClient.PostAlert(alert, serviceURL, proxyURL);

            Assert.IsNotNull(opStatus.OperationID);
        }

        [TestMethod]
        public void Scorch_Client_Returns_Result_2()
        {
            IScorchClient scorchClient = new ScorchClient();

            var alert = new SCOMAlert();

            alert.AlertId = Guid.NewGuid();
            alert.AlertDescription = @"Error scanning logfile /var/log/secure on host SAPPRDPG0DB01 as user <SCXUser><UserId>sccommon</UserId><Elev>sudo</Elev></SCXUser>; The SSL connection cannot be established. Verify that the service on the remote host is properly configured to listen for HTTPS requests. Consult the logs and documentation for the WS-Management service running on the destination, most commonly IIS or WinRM. If the destination is the WinRM service, run the following command on the destination to analyze and configure the WinRM service: winrm quickconfig -transport:https. \n";
            alert.TimeAdded = new DateTime(2015, 8, 27, 18, 12, 47  );
            alert.ManagementPackName = @"Microsoft.Unix.Library";
            alert.Priority = "Medium";
            alert.Severity = "Warning";
            alert.MonitoringObjectDisplayName = @"UGBGWSC1038.national.core.bbc.co.uk";
            alert.MonitoringObjectFullName = @"Microsoft.SystemCenter.HealthService:UGBGWSC1038.national.core.bbc.co.uk";
            alert.MonitoringObjectId = Guid.NewGuid();
            alert.MonitoringObjectPath = @"UGBGWSC1038.national.core.bbc.co.uk";
            alert.MonitoringObjectName = string.Empty;
            alert.Server = "UGBGWSC1038";
            alert.MessageId = Guid.NewGuid();
            alert.Name = @"Logfile module error event detected";
            alert.Status = 0;

            string proxyURL = @"http://www-cache.reith.bbc.co.uk:80/"; // ConfigurationManager.AppSettings["Proxy"].ToString();
            //string serviceURL = @"http://scom-connector.gateway.bbc.co.uk:85/api/scom"; // ConfigurationManager.AppSettings["ServiceURL"].ToString(); 
            string serviceURL = @"http://bgb01ws1011.national.core.bbc.co.uk:8090/api/scom"; // Co

            var opStatus = scorchClient.PostAlert(alert, serviceURL, proxyURL);

            Assert.IsNotNull(opStatus.OperationID);
        }

        [TestMethod]
        public void Scorch_Client_Serialize_And_Deserialize()
        {
            try
            {
                IScorchClient scorchClient = new ScorchClient();

                SCOMAlert alert = new SCOMAlert();
                alert.AlertId = Guid.NewGuid();
                alert.AlertDescription = "Test SCOM to ITSM alert";
                alert.Domain = "NATIONAL";
                alert.Name = "SCOM ALERT 01";
                alert.NetbiosComputerName = "NetbiosComputerName";
                alert.NetbiosDomainName = "NetbiosComputerDomain";
                alert.ManagementPackName = @"Microsoft.Windows.Server.2008.R2.Monitoring.BPA";
                alert.ManagementPackFriendlyName = @"Windows Server 2008 R2 Best Practice Analyzer Monitoring";
                alert.Owner = "URQUHD02";
                alert.PrincipalName = "David Urquhart";
                alert.Priority = "Medium";
                alert.ResolvedBy = string.Empty;
                alert.Severity = "Medium";
                alert.SiteName = "TVC";
                alert.TimeAdded = DateTime.Now;
                alert.TimeRaised = DateTime.Now.AddMinutes(-30d);
                alert.TimeResolved = null;


                var json = JsonConvert.SerializeObject(alert);

                var obj = JsonConvert.DeserializeObject<SCOMAlert>(json);

                Assert.IsNotNull(obj);
                Assert.IsInstanceOfType (obj, typeof(SCOMAlert));
            }
            catch (Exception e)
            {

            }
        }
    }
}
