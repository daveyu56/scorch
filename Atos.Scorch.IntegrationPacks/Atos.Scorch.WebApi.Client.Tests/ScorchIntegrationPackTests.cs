﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Atos.Scorch.IntegrationPacks;
using Atos.Scorch.Domain;


namespace Atos.Scorch.WebApi.Client.Tests
{
    [TestClass]
    public class ScorchIntegrationPackTests
    {
        [TestMethod]
        public void Scorch_Client_Returns_Result_From_Integration_Pack()
        {
            ScorchActivity activity = new ScorchActivity();

            //activity.AlertID = Guid.NewGuid();
            //activity.Description = "Test SCOM to ITSM alert";
            //activity.Domain = "NATIONAL";
            //activity.Name = "SCOM ALERT 01";
            //activity.NetbiosComputerName = "NetbiosComputerName";
            //activity.NetbiosDomainName = "NetbiosComputerDomain";
            //activity.Owner = "URQUHD02";
            //activity.PrincipalName = "David Urquhart";
            //activity.Priority = "Medium";
            //activity.ResolvedBy = string.Empty;
            //activity.Severity = "Medium";
            //activity.SiteName = "TVC";
            //activity.TimeAdded = DateTime.Now;
            //activity.TimeRaised = DateTime.Now.AddMinutes(-30d);
            //activity.TimeResolved = null;
            //activity.ManagementPackName = "Windows";
            

            activity.CreateITSMTicket();

            string incidentId = activity.IncidentID;
            string assignToGroup = activity.OutputResolutionState;
          
            Assert.IsNotNull(incidentId);
            Assert.IsNotNull(assignToGroup);
            Assert.AreEqual(assignToGroup, "Assigned To Engineering");
        }
    }
}
